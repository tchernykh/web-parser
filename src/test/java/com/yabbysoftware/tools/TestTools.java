package com.yabbysoftware.tools;

import com.yabby.htmlparser.IAction;
import com.yabby.htmlparser.YabbyHtmlParser;
import com.yabby.htmlparser.annotation.ARuleAnnotationProcessor;
import com.yabby.htmlparser.annotation.ARuleAnnotationProcessorHelper;
import com.yabby.htmlparser.impl.Parser;
import com.yabby.htmlparser.impl.ParserSettings;
import com.yabby.htmlparser.impl.jsoup.JsoupParser;
import com.yabby.htmlparser.processor.Rule;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaFileObject;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Tool methods for easy testing.
 */
public class TestTools {

    public static void parse(String fileName, Rule... rules) throws Exception {
        Parser parser = new JsoupParser();
        ParserSettings parserSettings = new ParserSettings();
        parserSettings.setParsingType(ParserSettings.ParsingType.ANDROID);
        parser.setParserSettings(parserSettings);
        InputStream stream = TestTools.class.getResourceAsStream(fileName);
        YabbyHtmlParser yabbyHtmlParser = new YabbyHtmlParser(parser, rules);
        yabbyHtmlParser.setDebug(false);
        yabbyHtmlParser.parse(stream);
        stream.close();
    }

    public static void parseRule(String fileName, IAction action, String rule) throws Exception {
        Parser parser = new JsoupParser();
        String ruleCodeString = ARuleAnnotationProcessorHelper.parseRootRule(rule);
        CharSequenceCompiler<Object> c = new CharSequenceCompiler<>(ClassLoader.getSystemClassLoader(), new ArrayList<String>());
        final DiagnosticCollector<JavaFileObject> errs = new DiagnosticCollector<>();
        String className = "TestClass";
        try {
//            System.out.println("code: " + ruleCodeString);
            String packageName = "com.yabby.htmlparser";
            Class<Object> clazz = c.compile(packageName + "." + className, getClassSource(className, ruleCodeString),
                    errs, Object.class);
            Rule r = (Rule) clazz.getMethod("getRule", IAction.class).invoke(null, action);
//            System.out.println("Rule: " + r);
            InputStream stream = TestTools.class.getResourceAsStream(fileName);
            YabbyHtmlParser yabbyHtmlParser = new YabbyHtmlParser(parser, r);
            yabbyHtmlParser.setDebug(false);
            yabbyHtmlParser.parse(stream);
            stream.close();
        } catch (CharSequenceCompilerException ex) {
            for (Diagnostic<? extends JavaFileObject> diagnostic : ex.getDiagnostics().getDiagnostics()) {
                System.err.println(diagnostic);
            }
            ex.printStackTrace();
        }
    }

    private static CharSequence getClassSource(String className, String ruleCodeString) {
        return "package com.yabby.htmlparser;\n" +
                ARuleAnnotationProcessor.getImports() +
                "public class " + className + " {" +
                "public static Rule getRule(IAction action) {" +
                "return new Rule(action, " + ruleCodeString + ");" +
                "}" +
                "}";
    }

    public static void parseRule(String fileName, IAction action, String parentRule, String rule) throws Exception {
        Parser parser = new JsoupParser();
        String parentRuleCodeString = ARuleAnnotationProcessorHelper.parseRootRule(parentRule);
        String ruleCodeString = ARuleAnnotationProcessorHelper.parseRootRule(rule);
        CharSequenceCompiler<Object> c = new CharSequenceCompiler<>(ClassLoader.getSystemClassLoader(), new ArrayList<String>());
        final DiagnosticCollector<JavaFileObject> errs = new DiagnosticCollector<>();
        String className = "TestClass";
        try {
//            System.out.println("code: " + ruleCodeString);
            String packageName = "com.yabby.htmlparser";
            CharSequence classSource = getClassSource(className, parentRuleCodeString, ruleCodeString);
//            System.out.println("class: " + classSource);
            Class<Object> clazz = c.compile(packageName + "." + className, classSource,
                    errs, Object.class);
            Rule r = (Rule) clazz.getMethod("getRule", IAction.class).invoke(null, action);
//            System.out.println("Rule: " + r);
            InputStream stream = TestTools.class.getResourceAsStream(fileName);
            YabbyHtmlParser yabbyHtmlParser = new YabbyHtmlParser(parser, r);
            yabbyHtmlParser.setDebug(false);
            yabbyHtmlParser.parse(stream);
            stream.close();
        } catch (CharSequenceCompilerException ex) {
            for (Diagnostic<? extends JavaFileObject> diagnostic : ex.getDiagnostics().getDiagnostics()) {
                System.err.println(diagnostic);
            }
            ex.printStackTrace();
        }
    }

    private static CharSequence getClassSource(String className, String parentRuleString, String ruleCodeString) {
        return "package com.yabby.htmlparser;\n" +
                ARuleAnnotationProcessor.getImports() +
                "public class " + className + " {" +
                "    public static Rule getRule(IAction action) {" +
                "        Rule parentRule = new Rule(null, " + parentRuleString + "); " +
                "        return new Rule(action, parentRule, " + ruleCodeString + ");" +
                "    }" +
                "}";
    }
}
