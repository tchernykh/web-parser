package com.yabbysoftware.socialnetwork.googleplus;

import com.yabby.htmlparser.YabbyHtmlParser;
import com.yabby.htmlparser.actions.Action;
import com.yabby.htmlparser.impl.HtmlNode;
import com.yabby.htmlparser.impl.ParserSettings;
import com.yabby.htmlparser.impl.jsoup.JsoupParser;
import com.yabby.htmlparser.processor.Rule;
import com.yabby.htmlparser.rule.AndRuleItem;
import com.yabby.htmlparser.rule.AnyNode;
import com.yabby.htmlparser.rule.TagWithAttributeRuleItem;
import com.yabbysoftware.tools.TestTools;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Unit test for GPlusParser class
 */
public class GPlusParserTest {

    @Test
    public void testGPlusLoginParse() throws Exception {
        GPlusParser parser = new GPlusParser();
        Map<String, String> data = parser.parseLoginPage(getClass().getResourceAsStream("/googleplus/login/page1.html"));
        assertNotNull(data);
        assertFalse(data.isEmpty());
    }

    @Test
    public void testGPlusPostParse() throws Exception{
        GPlusParser parser = new GPlusParser();
        List<GPlusPost> result = parser.parsePostsPage(getClass().getResourceAsStream("/googleplus/googleplus_mobile.html"));
        for (GPlusPost gPlusPost : result) {
            System.out.println("post: " + gPlusPost);
        }
        assertNotNull(result);
        assertEquals(18, result.size());

        GPlusPost first = result.get(0);
        assertEquals("z120dbp4btbqv3rve22hwjrxfxfjtft3c", first.id);
        assertNull(first.category);
        assertEquals(32, first.likesCount);
        assertEquals(1, first.sharesCount);
        assertEquals(6, first.commentsCount);
        assertEquals("Ashrafee Oishi", first.topCommentAuthor);
        assertEquals("Haha:-D", first.topCommentDescription);
        assertEquals("113308995872881012789", first.authorId);
        assertEquals("James Rothwell", first.authorName);
        assertEquals("9 hours ago", first.createdAt);
        assertEquals("VinceVaughn", first.tag);
        assertTrue(first.content.startsWith("Haha this is so awesome"));
        assertEquals("//lh3.googleusercontent.com/-49lk4wNy3MY/VPZd4O80f1I/AAAAAAAAhfU/xyAZi2Y20BY/w1280-h800-p-k/iStock-Unfinished-Business-2.jpg", first.mediaContentImage);

        GPlusPost second = result.get(1);
        assertEquals("z131intahp3vzfg2q22rvrnrssbsupkdv", second.id);
        assertEquals("Hot on Google+", second.category);
        assertEquals("15 hours ago", second.createdAt);
        assertEquals("NIGERIAN", second.tag);
        assertTrue(second.content.contains("DRIVERS SHOULD THANK PRESIDENT"));
        assertEquals("//lh6.googleusercontent.com/-xH-CXl4HqKg/VPX8o41d_DI/AAAAAAAAE8E/0eoDAi4wGcE/w1280-h800-p-k/Roads%2BTrans%2BCollage.jpg", second.mediaContentImage);

        for (GPlusPost gPlusPost : result) {
            if (gPlusPost.mediaContentYoutube != null) return;
        }
        fail("Youtube content wasn't found");
    }

    @Test
    public void testParseStartPageFirst() throws Exception {
        GPlusParser parser = new GPlusParser();
        long startTime = System.currentTimeMillis();
        List<GPlusPost> posts = parser.parsePostsPage(getClass().getResourceAsStream("/googleplus/startpage/first.html"));
        System.out.println("Parse time: " + (System.currentTimeMillis() - startTime));
        // first and second posts are simple posts with text and image
        // first - image is animated gif
        assertFirstLoadedPost(posts.get(0));
        assertSecondLoadedPost(posts.get(1));
        // not check third post - nothing new to check there
        assertFourthLoadedPost(posts.get(3));
        // posts 4-9 not interested - nothing new to check, tenth is reshare post that we particulary check in another test
        // all other posts not interesting as well
        assertEquals(18, posts.size());
    }

    private void assertFirstLoadedPost(GPlusPost first) {
        assertEquals("101560853443212199687", first.authorId);
        assertEquals("Google+", first.authorName);
        assertNull(first.category);
        // &nbsp; should be converted to \u00a0 symbol
        assertEquals("Be ready in a sec, just have to do my hair. " +
                "<a href=\"/app/basic/s?cbp=15izylcokmcwj&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23HappyFriday\">#HappyFriday</a> " +
                "<a href=\"/app/basic/s?cbp=15izylcokmcwj&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23tGIF\">#tGIF</a> <br/><br/>h/t to " +
                "+<a href=\"/app/basic/106389313478618258947/posts?cbp=15izylcokmcwj&sview=2&cid=5&soc-app=115&soc-platform=1&spath=/app/basic/stream\">TwistedSifter</a>\u00a0for this top notch gif.", first.content);
        assertEquals("10 hours ago", first.createdAt);
        assertEquals("z13xujpyyxfrvvr2a22xc52a3mmtwzkr004", first.id);
        assertNull(first.mediaContentDescription);
        assertEquals("//lh3.googleusercontent.com/-sGSnlKIU6c0/VPnVwqSDrJI/AAAAAAAA21E/f8HO6_p8BZ8/w1280-h800-p-k/vqjlI1G.gif", first.mediaContentImage);
        assertNull(first.mediaContentLink);
        assertEquals("/app/basic/photos/101560853443212199687/album/6123160189460082769/6123160200347364498?cbp=1umwubfc94nlr&sview=2&cid=5&soc-app=115&soc-platform=1&spath=/app/basic/stream", first.mediaContentMoreLink);
        assertNull(first.mediaContentMore);
        assertNull(first.mediaContentYoutube);
        assertEquals("tGIF", first.tag);
        // Unable to check line below because of encoding issues during compilation
        // assertEquals("Reѕнмα Sĸ", first.topCommentAuthor);
        assertEquals("hahahaha........", first.topCommentDescription);
        assertEquals(185, first.commentsCount);
        assertEquals(4197, first.likesCount);
        assertEquals(792, first.sharesCount);
    }

    private void assertSecondLoadedPost(GPlusPost second) {
        assertEquals("z120jdcrcmyltvbmq22sevx42se4cncaw04", second.id);
        assertEquals("Hot on Google+", second.category);
        assertEquals("106583955549153144644", second.authorId);
        assertEquals("comfortable food", second.authorName);
        assertEquals("Food", second.tag);
        assertEquals("2 hours ago", second.createdAt);

        // &nbsp; should be converted to \u00a0 symbol
        assertEquals("This <b>Lemon Blueberry Pound Cake</b> is perfectly dense and sweet and tart. It's the perfect breakfast bread.\u00a0<br/><br/>Here's how to make it:<br/>" +
                "<a href=\"http://comfortablefood.com/lemon-blueberry-pound-cake/\">http://comfortablefood.com/lemon-blueberry-pound-cake/</a><br/><br/> " +
                "<a href=\"/app/basic/s?cbp=15izylcokmcwj&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23cake\">#cake</a> " +
                "<a href=\"/app/basic/s?cbp=15izylcokmcwj&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23bread\">#bread</a> " +
                "<a href=\"/app/basic/s?cbp=15izylcokmcwj&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23recipes\">#recipes</a> " +
                "<a href=\"/app/basic/s?cbp=15izylcokmcwj&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23food\">#food</a> " +
                "<a href=\"/app/basic/s?cbp=15izylcokmcwj&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23baking\">#baking</a> " +
                "<a href=\"/app/basic/s?cbp=15izylcokmcwj&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23foodblog\">#foodblog</a> " +
                "<a href=\"/app/basic/s?cbp=15izylcokmcwj&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23brunch\">#brunch</a>", second.content);

        assertNull(second.mediaContentDescription);
        assertEquals("//lh4.googleusercontent.com/-Q1aTtH2WYCI/VPpE2mI7dlI/AAAAAAAAONY/XwLxiXNrPek/w1280-h800-p-k/lemon_blueberry_pound_cake1.jpg", second.mediaContentImage);
        assertNull(second.mediaContentLink);
        assertEquals("/app/basic/photos/106583955549153144644/album/6123282350242354401/6123282348105234002?cbp=1umwubfc94nlr&sview=2&cid=5&soc-app=115&soc-platform=1&spath=/app/basic/stream", second.mediaContentMoreLink);
        assertNull(second.mediaContentMore);
        assertNull(second.mediaContentYoutube);
        assertEquals("Valerie Saundera", second.topCommentAuthor);
        assertEquals("What is the recipe", second.topCommentDescription);
        assertEquals(150, second.likesCount);
        assertEquals(23, second.sharesCount);
        assertEquals(43, second.commentsCount);
    }

    private void assertFourthLoadedPost(GPlusPost fourth) {
        assertEquals("z12lzvsopxz3jv0a104cixchgyedj5bh1xs0k", fourth.id);
        assertNull(fourth.category);
        assertEquals("111395306401981598462", fourth.authorId);
        assertEquals("Google Developers", fourth.authorName);
        assertEquals("CompressorHead", fourth.tag);
        assertEquals("6 hours ago", fourth.createdAt);

        // &nbsp; should be converted to \u00a0 symbol
        assertEquals("<b>New Compressor Head: Shining light on arithmetic compression</b><br/><br/>In episode 5 of <b>Compressor Head</b>, +" +
                "<a href=\"/app/basic/105062545746290691206/posts?cbp=15izylcokmcwj&sview=2&cid=5&soc-app=115&soc-platform=1&spath=/app/basic/stream\">Colt McAnlis</a>" +
                " sheds light on the dark mysteries of arithmetic compression. Colt walks us through the process of the algorithm, encoding and decoding using nothing but a low-odor dry-erase marker he found in Google\u2019s garage. Also he has a lava lamp.<br/><br/> " +
                "<a href=\"/app/basic/s?cbp=15izylcokmcwj&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23CompressorHead\">#CompressorHead</a> " +
                "<a href=\"/app/basic/s?cbp=15izylcokmcwj&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23PERFMATTERS\">#PERFMATTERS</a>", fourth.content);

        assertEquals("//lh6.googleusercontent.com/proxy/tGdPUBjMLxnaB53fPoGiqLl8mq-qp5G0ckET2AdnLz5mz-pUFpHhymJDhV7piJT3mtcUIN6dxCzge1zmJHHytyTJ3P0=w1280-h800-o-k", fourth.mediaContentImage);
        assertEquals("http://www.youtube.com/watch?v=FdMoL3PzmSA", fourth.mediaContentLink);
        assertNull(fourth.mediaContentMoreLink);
        assertNull(fourth.mediaContentMore);
        assertNull(fourth.mediaContentYoutube);
        assertEquals("Arithmetic Compression (Ep 5, Compressor Head) Google", fourth.mediaContentDescription);
        // Unable to check line below because of encoding issues during compilation
        assertEquals("Richard Zipperer", fourth.topCommentAuthor);
        assertEquals("+<a href=\"/app/basic/110807017352441925151/posts?cbp=15izylcokmcwj&sview=2&cid=5&soc-app=115&soc-platform=1&spath=/app/basic/stream\">Andreas Genelin</a> what's with my half of alibaba? You have an interest or investment with Jerry?", fourth.topCommentDescription);
        assertEquals(62, fourth.likesCount);
        assertEquals(24, fourth.sharesCount);
        assertEquals(3, fourth.commentsCount);
    }

    @Test
    public void testParseStartPageReshare() throws Exception {
        GPlusParser parser = new GPlusParser();
        List<GPlusPost> posts = parser.parsePostsPage(getClass().getResourceAsStream("/googleplus/startpage/post_reshare_only.html"));
        assertEquals(1, posts.size());
        GPlusPost post = posts.get(0);
        assertEquals("z12sd3qgsrq5cjuwl04cixchgyedj5bh1xs0k", post.id);
        assertNull(post.category);
        assertEquals("111395306401981598462", post.authorId);
        assertEquals("Google Developers", post.authorName);
        assertEquals("CoffeewithaGoogler", post.tag);
        assertEquals("4 hours ago", post.createdAt);
        assertEquals("Join +<a href=\"/app/basic/108759069548186989918/posts?cbp=15izylcokmcwj&sview=2&cid=5&soc-app=115&soc-platform=1&spath=/app/basic/stream\">Laurence Moroney</a> for another episode of " +
                "<a href=\"/app/basic/s?cbp=15izylcokmcwj&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23CoffeewithaGoogler\">#CoffeewithaGoogler</a> . In today's video, he chats with +" +
                "<a href=\"/app/basic/112600205125796554588/posts?cbp=15izylcokmcwj&sview=2&cid=5&soc-app=115&soc-platform=1&spath=/app/basic/stream\">Fred Chung</a> about how Google thinks about reaching developers with tools, sdks, documentation & more!", post.reshareContent);
        assertNull(post.mediaContentYoutube);
        assertNull(post.mediaContentMoreLink);
        assertNull(post.mediaContentMore);
        // Unable to check line below because of encoding issues during compilation
        assertNull(post.topCommentAuthor);
        assertNull(post.topCommentDescription);
        assertEquals(27, post.likesCount);
        assertEquals(4, post.sharesCount);
        assertEquals(0, post.commentsCount);
        assertEquals("108967384991768947849", post.resharedAuthorId);
        assertEquals("Android Developers", post.resharedAuthorName);
        assertEquals("http://www.youtube.com/watch?v=KDb4SyFtylU", post.mediaContentLink);
        assertEquals("//lh6.googleusercontent.com/proxy/litgDsS8Jg8fhM0fwQR3r61uSkTBKKJOXry52LOF3x-dlT0fn4jC6qj0Z3MrVTw5mEhDLaxbF0XaUYE545q75SRKLls=w1280-h800-o-k", post.mediaContentImage);
        assertEquals("Coffee with a Googler: Chat with Fred Chung about developer advocacy", post.mediaContentDescription);
    }

//    @Test
//    public void testParseLoginVerifyPhoneNumberPage() throws Exception {
//        RootPageParser parser = new RootPageParser();
//        try {
//            Document doc = Jsoup.parse(getClass().getResourceAsStream("/login_verify_phone_number.html"), "UTF-8", GPlusLoader.GOOGLE_PLUS_URL);
//            parser.parse(doc);
//            fail("VerifyNumberPageException should happen");
//        } catch (RootPageParser.VerifyNumberPageException vnpe) {
//            assertEquals("/app/basic/settings/phone/edit?cbp=hxx2zkhm7m23&sview=31&awwisf=1&cid=5&soc-app=115&soc-platform=1", vnpe.getFormPostUrl());
//            assertEquals("Australia", vnpe.getCountryName());
//            Map<String, String> formParams = vnpe.getFormPostValues(RootPageParser.VerifyNumberPageException.ACTION_NEXT);
//            assertNotNull(formParams);
//            assertEquals(5, formParams.size());
//            assertEquals(String.valueOf(RootPageParser.VerifyNumberPageException.ACTION_NEXT), formParams.get("buttonPressed"));
//            assertEquals("AObGSAhE11GNXILAHzPRJ2XlnZHjLNBSdg:1425645552291", formParams.get("at"));
//            assertEquals("CgIIABISCglBdXN0cmFsaWESAys2MRoA", formParams.get("aggregatedData"));
//            assertEquals("4", formParams.get("currentPage"));
//            assertNull(formParams.get("spePostNum"));
//        }
//    }

    @Test
    public void testParseStartPages() throws Exception {
        GPlusParser parser = new GPlusParser();
        long startTime = System.currentTimeMillis();
        List<GPlusPost> posts1 = parser.parsePostsPage(getClass().getResourceAsStream("/googleplus/startpage/first.html"));
        assertNotNull(parser.getNextPageUrl());
        List<GPlusPost> posts2 = parser.parsePostsPage(getClass().getResourceAsStream("/googleplus/startpage/second.html"));
        assertNotNull(parser.getNextPageUrl());
        List<GPlusPost> posts3 = parser.parsePostsPage(getClass().getResourceAsStream("/googleplus/startpage/third.html"));
        assertNotNull(parser.getNextPageUrl());
        List<GPlusPost> posts4 = parser.parsePostsPage(getClass().getResourceAsStream("/googleplus/startpage/fourth.html"));
        assertNotNull(parser.getNextPageUrl());
        List<GPlusPost> posts5 = parser.parsePostsPage(getClass().getResourceAsStream("/googleplus/startpage/fifth.html"));
        assertNotNull(parser.getNextPageUrl());
        assertEquals(18, posts1.size());
        assertEquals(18, posts2.size());
        assertEquals(18, posts3.size());
        assertEquals(18, posts4.size());
        assertEquals(18, posts5.size());
        for (GPlusPost gPlusPost : posts1) {
            assertPost(gPlusPost);
        }
        for (GPlusPost gPlusPost : posts2) {
            assertPost(gPlusPost);
        }
        for (GPlusPost gPlusPost : posts3) {
            assertPost(gPlusPost);
        }
        for (GPlusPost gPlusPost : posts4) {
            assertPost(gPlusPost);
        }
        for (GPlusPost gPlusPost : posts5) {
            assertPost(gPlusPost);
        }
        System.out.println("Parse time: " + (System.currentTimeMillis() - startTime));
    }

    private void assertPost(GPlusPost gPlusPost) {
        assertNotNull("post id null: " + gPlusPost, gPlusPost.id);
        assertNotNull("post authorId null: " + gPlusPost, gPlusPost.authorId);
        assertNotNull("post authorName null: " + gPlusPost, gPlusPost.authorName);
        assertNotNull("post createdAt null: " + gPlusPost, gPlusPost.createdAt);
    }

    @Test
    public void testRule() throws Exception {
        TestTools.parseRule("/googleplus/stream/first.html", new Action() {
                    @Override
                    public void action(HtmlNode htmlNode, int level, Rule rule) {
                        System.out.println("node " + htmlNode.getText());
                    }
                }, "*/div(class='fHa')&div(data-itemid);",
                "*/div(class='taqksc')/*/button(class='c4QUad')");
    }

    @Test
    public void testRuleManual() throws Exception {
        JsoupParser parser = new JsoupParser();
        ParserSettings parserSettings = new ParserSettings();
        parserSettings.setParsingType(ParserSettings.ParsingType.ANDROID);
        parser.setParserSettings(parserSettings);

        Rule rootRule = new Rule(new Action() {
            @Override
            public void action(HtmlNode node, int level, Rule rule) {
                System.out.println("node " + node.getText());
            }
        }, new AnyNode(), new AndRuleItem(new TagWithAttributeRuleItem("div", "class", "fHa"),new TagWithAttributeRuleItem("div", "data-itemid")));
//        rootRule.setStopOnAction(true);

        Rule rule = new Rule(new Action() {
            @Override
            public void action(HtmlNode node, int level, Rule rule) {
                System.out.println("node " + node.getText());
            }
        }, rootRule, new AnyNode(), new TagWithAttributeRuleItem("div", "class", "uFb"));

        YabbyHtmlParser yabbyHtmlParser = new YabbyHtmlParser<>(parser, rule);
        yabbyHtmlParser.parse(getClass().getResourceAsStream("/googleplus/stream/first.html"));
    }

    @Test
    public void testParseFirstPage() throws Exception {
//        GPlusParser parser = new GPlusParser();
//        YabbyHtmlParser<GPlusPost> yabbyHtmlParser = new YabbyHtmlParser<>(parser.getParser(), new ManualGPlusPostYabbyHtmlParserProcessor());
//        InputStream stream = getClass().getResourceAsStream("/googleplus/stream/first.html");
//        List<GPlusPost> posts = yabbyHtmlParser.parse(stream);
//        stream.close();

        GPlusParser parser = new GPlusParser();
        List<GPlusPost> posts = parser.parsePostsPage(getClass().getResourceAsStream("/googleplus/stream/first.html"));
        assertEquals(18, posts.size());

        GPlusPost firstPost = posts.get(0);
        assertEquals("z13nx51hhl2utvlvd23re3fbmmftdvcji04", firstPost.id);
        assertEquals("Google Developers", firstPost.authorName);
        assertEquals("111395306401981598462", firstPost.authorId);
        assertEquals("11 hours ago", firstPost.createdAt);
        assertEquals("<b>100 days of Google Dev, Episode 13/100</b> <br/><br/>" +
                        "Optimise your app’s performance in search results and get found on Google.<br/> <br/>" +
                        "<a href=\"/app/basic/s?cbp=zkv4lj142zig&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23GoogleDev100\">#GoogleDev100</a><br/><br/>" +
                        "<a href=\"https://goo.gl/m1ZIu8\">https://goo.gl/m1ZIu8</a>",
                firstPost.content);
        assertEquals("https://goo.gl/FCrHVg", firstPost.mediaContentLink);
        assertEquals("//lh3.googleusercontent.com/proxy/ux-ltFWCnsz2Kmu1HQuEPH6wxINIvqCMB0lbvwfQp0D1Fll6WnWsglxU2vB2iXAXSlhdOv-85Da8mnVs-2KOow1BOq4=w1280-h800-o-k", firstPost.getMediaContentImage());
        assertEquals("Get your app found on Google (100 Days of Google Dev)", firstPost.mediaContentDescription);
        assertEquals(92, firstPost.likesCount);
        assertEquals(26, firstPost.sharesCount);
        assertEquals(1, firstPost.commentsCount);
        assertEquals("ignacio gonzalez-outon", firstPost.topCommentAuthor);
        assertEquals("Clinica dental ignacio González-outon Velazquez", firstPost.topCommentDescription);

        GPlusPost secondPost = posts.get(1);
        assertEquals("z121jlgqzm22ulllx230y3d45vyagtcqh04", secondPost.id);
        assertEquals("Hot on Google+", secondPost.category);
        assertEquals("106456488116418463849", secondPost.authorId);
        assertEquals("1 hour ago", secondPost.createdAt);
        assertEquals("Goodnight everyfurr :3 <br/>" +
                "I leave you with the husky shark! <br/><br/><br/>" +
                "<a href=\"/app/basic/s?cbp=zkv4lj142zig&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23animals\">#animals</a> " +
                "<a href=\"/app/basic/s?cbp=zkv4lj142zig&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23husky\">#husky</a> " +
                "<a href=\"/app/basic/s?cbp=zkv4lj142zig&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23lol\">#lol</a> " +
                "<a href=\"/app/basic/s?cbp=zkv4lj142zig&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23cute\">#cute</a> " +
                "<a href=\"/app/basic/s?cbp=zkv4lj142zig&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23water\">#water</a> " +
                "<a href=\"/app/basic/s?cbp=zkv4lj142zig&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23pool\">#pool</a> " +
                "<a href=\"/app/basic/s?cbp=zkv4lj142zig&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23swimming\">#swimming</a> " +
                "<a href=\"/app/basic/s?cbp=zkv4lj142zig&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23dog\">#dog</a> " +
                "<a href=\"/app/basic/s?cbp=zkv4lj142zig&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23canine\">#canine</a>", secondPost.content);
        assertEquals("//lh3.googleusercontent.com/-2bQZxfpkCVc/VXztZO1YqUI/AAAAAAAAaBE/strOqkVI5Vo/w1280-h800-p-k/2015%252520-%2525201%252520%2528%2525D9%2525A3%2525D9%2525A2%2529.jpg", secondPost.mediaContentImage);
        assertEquals(515, secondPost.likesCount);
        assertEquals(35, secondPost.sharesCount);
        assertEquals(0, secondPost.commentsCount);
        assertEquals("Zufan Atsbeha", secondPost.topCommentAuthor);
        assertEquals("Wow nice", secondPost.topCommentDescription);

        GPlusPost sixthPost = posts.get(5);
        assertEquals("z13uefghyrmwvxcra04cfp0ocsiefbny2w00k", sixthPost.id);
        assertEquals("101560853443212199687", sixthPost.authorId);
        assertEquals("Google+", sixthPost.authorName);
        assertEquals("1 day ago", sixthPost.createdAt);
        assertEquals("<b>Nick Woodard - World Champion Jump Roper</b><br/><br/>" +
                "+<a href=\"/app/basic/109154124866654719813/posts?cbp=zkv4lj142zig&sview=2&cid=5&soc-app=115&soc-platform=1&spath=/app/basic/stream\">BeyondSlowMotion</a> " +
                "Nick Woodard 7 time world champion and considered the best freestyle rump roper in the world.<br/><br/>" +
                "<b>Watch the video here</b>: <a href=\"http://ow.ly/O2TNy\">http://ow.ly/O2TNy</a><br/>" +
                "Subscribe to <b>BeyondSlowMotion</b> <a href=\"/app/basic/s?cbp=zkv4lj142zig&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23Youtube\">#Youtube</a> " +
                "here: <a href=\"http://ow.ly/O2TUl\">http://ow.ly/O2TUl</a><br/>" +
                "<b>Pin</b> it to view later: <a href=\"http://ow.ly/O37tx\">http://ow.ly/O37tx</a> <br/><br/>" +
                "<a href=\"/app/basic/s?cbp=zkv4lj142zig&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23Gifs\">#Gifs</a> " +
                "<a href=\"/app/basic/s?cbp=zkv4lj142zig&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23JumpRope\">#JumpRope</a> " +
                "<a href=\"/app/basic/s?cbp=zkv4lj142zig&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23BeyondSlowMotion\">#BeyondSlowMotion</a> " +
                "<a href=\"/app/basic/s?cbp=zkv4lj142zig&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23VideoPromtion\">#VideoPromtion</a> <a href=\"/app/basic/s?cbp=zkv4lj142zig&sview=2&cid=5&soc-app=115&soc-platform=1&sq=%23GifYourVid\">#GifYourVid</a>", sixthPost.content);
        assertEquals("Jumping rope is hard enough on its own. <a href=\"https://goo.gl/u0tzc1\">https://goo.gl/u0tzc1</a>", sixthPost.reshareContent);
        assertEquals("110582789908716484457", sixthPost.resharedAuthorId);
        assertEquals("Gif your Vid", sixthPost.resharedAuthorName);
    }

    @Test
    public void testParse6and7Pages() throws Exception {
        GPlusParser parser = new GPlusParser();
        List<GPlusPost> s7Posts = parser.parsePostsPage(getClass().getResourceAsStream("/googleplus/stream/s7.html"));
        assertEquals(18, s7Posts.size());

        List<GPlusPost> s8Posts = parser.parsePostsPage(getClass().getResourceAsStream("/googleplus/stream/s8.html"));
        assertEquals(20, s8Posts.size());
    }
}
