package com.yabbysoftware.socialnetwork.googleplus;

import com.yabbysoftware.network.SerializableCookie;
import org.junit.Test;

import java.io.*;
import java.net.HttpCookie;
import java.util.*;

import static org.junit.Assert.*;

/**
 * Test for GPlusNetwork
 */
public class GPlusNetworkTest {

    private Properties props;

    public String getProperty(String name) throws Exception {
        String res = System.getProperty(name);
        if (res == null) {
            if (props == null) {
                props = new Properties();
                props.load(getClass().getClassLoader().getResourceAsStream("test.properties"));
            }
            res = props.getProperty(name);
        }
        return res;
    }

    @Test
    public void testLoading() throws Exception {
        if (Boolean.valueOf(getProperty("networkTests"))) {
            GPlusNetwork gPlusNetwork = new GPlusNetwork();
            GPlusCredentials gPlusCredentials = new GPlusCredentials();
            gPlusCredentials.setUsername(getProperty("gPlusUsername"));
            gPlusCredentials.setPassword(getProperty("gPlusPassword"));
            gPlusNetwork.login(gPlusCredentials);

            loadAndAssertGplusPosts(gPlusNetwork);
        }
    }

    @Test
    public void testSavingStoredCookies() throws Exception {
        if (Boolean.valueOf(getProperty("networkTests"))) {
            GPlusNetwork gPlusNetwork = new GPlusNetwork();
            GPlusCredentials gPlusCredentials = new GPlusCredentials();
            gPlusCredentials.setUsername(getProperty("gPlusUsername"));
            gPlusCredentials.setPassword(getProperty("gPlusPassword"));
            List<HttpCookie> cookies = gPlusNetwork.login(gPlusCredentials);

            String fileAddr = getProperty("gPlusCookiesFile");
            File cookiesFile = new File(fileAddr);
            if (cookiesFile.exists()) {
                cookiesFile.delete();
            }
            FileOutputStream fout = new FileOutputStream(cookiesFile);
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(SerializableCookie.toSerializable(cookies));
            oos.close();
        }
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testLoadingUsingStoredCookies() throws Exception {
        if (Boolean.valueOf(getProperty("networkTests"))) {
            try (FileInputStream fin = new FileInputStream(getProperty("gPlusCookiesFile"))) {
                ObjectInputStream ois = new ObjectInputStream(fin);
                List<SerializableCookie> serializableCookies = (List<SerializableCookie>) ois.readObject();

                GPlusNetwork gPlusNetwork = new GPlusNetwork();
                gPlusNetwork.login(SerializableCookie.fromSerializable(serializableCookies));
                loadAndAssertGplusPosts(gPlusNetwork);
            }
        }
    }

    public static void loadAndAssertGplusPosts(GPlusNetwork gPlusNetwork) throws Exception {
        Set<String> authorsIds = new HashSet<>();
        List<GPlusPost> firstPage = new ArrayList<>(gPlusNetwork.loadLatestPosts());
        assertNotNull(firstPage);
        assertTrue(firstPage.size() > 0);
        for (GPlusPost post : firstPage) {
            assertNotNull(post.getId());
            assertNotNull(post.getAuthorId());
            assertNotNull(post.getAuthorName());
            assertNotNull(post.getCreatedAt());
            authorsIds.add(post.getAuthorId());
        }

        List<GPlusPost> secondPage = new ArrayList<>(gPlusNetwork.loadLatestPosts());
        assertNotNull(secondPage);
        assertTrue(secondPage.size() > 0);
        for (GPlusPost post : secondPage) {
            assertNotNull(post.getId());
            assertNotNull(post.getAuthorId());
            assertNotNull(post.getAuthorName());
            assertNotNull(post.getCreatedAt());
            authorsIds.add(post.getAuthorId());
        }

        GPlusPost postFromFirstPage = firstPage.get(0);
        GPlusPost postFromSecondPage = secondPage.get(0);
        assertNotEquals(postFromFirstPage.getId(), postFromSecondPage.getId());
        assertNotEquals(postFromFirstPage.getContent(), postFromSecondPage.getContent());

        for (String authorsId : authorsIds) {
            GPlusAuthor author = gPlusNetwork.loadAuthor(authorsId);
            assertNotNull(author);
            assertNotNull(author.getAuthorId());
            assertNotNull(author.getAuthorPhoto());
            assertNotNull(author.getAuthorName());
            assertNotNull(author.getAuthorCover());
        }
    }
}
