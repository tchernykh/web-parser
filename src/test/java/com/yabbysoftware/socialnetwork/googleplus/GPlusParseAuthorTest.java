package com.yabbysoftware.socialnetwork.googleplus;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Test for parsing Author page
 */
public class GPlusParseAuthorTest {
    @Test
    public void testParseAuthorGooglePage() throws Exception {
        GPlusParser parser = new GPlusParser();
        GPlusAuthor author = parser.parseAuthorPage(getClass().getResourceAsStream("/googleplus/author/google.html"));
        assertNotNull(author);
        assertEquals("116899029375914044550", author.authorId);
        assertEquals("//lh3.googleusercontent.com/-v0soe-ievYE/AAAAAAAAAAI/AAAAAAADJ90/sBXzzqsO9gY/s208-p-no/photo.jpg", author.authorPhoto);
        assertEquals("https://lh3.googleusercontent.com/-_O8wK1KAEs4/VUPPkbvfGvI/AAAAAAADCpo/-wplcDi4nGg/s630-fcrop64=1,00550000ffa9ffff/Beutler_Google_socialheaders-v1g%252B.png", author.authorCover);
        assertEquals("Google", author.authorName);
        assertEquals("http://www.google.com", author.authorWebsite);
    }
}
