package com.yabbysoftware.socialnetwork.facebook;

import com.yabby.htmlparser.actions.Action;
import com.yabby.htmlparser.YabbyHtmlParser;
import com.yabby.htmlparser.impl.HtmlNode;
import com.yabby.htmlparser.impl.ParserSettings;
import com.yabby.htmlparser.impl.jsoup.JsoupParser;
import com.yabby.htmlparser.index.ChildIndex;
import com.yabby.htmlparser.index.CurrentIndex;
import com.yabby.htmlparser.index.Index;
import com.yabby.htmlparser.processor.Rule;
import com.yabby.htmlparser.rule.AnyNode;
import com.yabby.htmlparser.rule.IndexedRuleItem;
import com.yabby.htmlparser.rule.TagRuleItem;
import com.yabby.htmlparser.rule.TagWithAttributeRuleItem;
import com.yabbysoftware.tools.TestTools;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Test for facebook parser
 */
public class FacebookParserTest {

    @Test
    public void testFacebookLoginParse() throws Exception {
        FacebookParser parser = new FacebookParser();
        Map<String, String> data = parser.parseLoginPage(getClass().getResourceAsStream("/facebook/login.html"));
        assertNotNull(data);
        assertFalse(data.isEmpty());
    }

    @Test
    public void testRule() throws Exception {
        TestTools.parseRule("/facebook/author/androidauthority.html", new Action() {
            @Override
            public void action(HtmlNode htmlNode, int level, Rule rule) {
                System.out.println("node " + htmlNode.getText());
            }
        }, "*/div(id='root')/*/div(id='structured_composer_async_container')/*/a(contains(href, 'v=timeline')).href.between('/','?')");
//        System.out.println("");
//        TestTools.parseRule("/facebook/r1_first.html", new Action() {
//            @Override
//            public void action(HtmlNode node, int level, Rule rule) {
//                System.out.println("node " + node.getText());
//            }
//        }, "*/div(id='viewport')/*/div(id='m_newsfeed_stream')/div(id='m-top-of-feed')[^1]/div(id)/div(id)", "[1][2]");
    }

    @Test
    public void testRuleManual() throws Exception {
        JsoupParser parser = new JsoupParser();
        ParserSettings parserSettings = new ParserSettings();
        parserSettings.setParsingType(ParserSettings.ParsingType.ANDROID);
        parser.setParserSettings(parserSettings);

        Rule rootRule = new Rule(new Action() {
            @Override
            public void action(HtmlNode node, int level, Rule rule) {
                System.out.println("node found: " + node.getText());
            }
        },
                new AnyNode(),
                new TagWithAttributeRuleItem("div", "id", "viewport"),
                new AnyNode(),
                new TagWithAttributeRuleItem("div", "id", "m_newsfeed_stream"),
                new IndexedRuleItem(new TagWithAttributeRuleItem("div", "id", "m-top-of-feed"), new CurrentIndex(1)),
                new TagRuleItem("div"));

        Rule childRule = new Rule(new Action() {
            @Override
            public void action(HtmlNode node, int level, Rule rule) {
                System.out.println("child node found: " + node.getText());
            }
        }, rootRule, new Index[] {new ChildIndex(1)}, new AnyNode(), new TagRuleItem("strong"), new TagRuleItem("a"));

        YabbyHtmlParser yabbyHtmlParser = new YabbyHtmlParser<>(parser, childRule);
        yabbyHtmlParser.parse(getClass().getResourceAsStream("/facebook/first.html"));
    }

    @Test
    public void testFacebookFirstPageParse() throws Exception {
        FacebookParser parser = new FacebookParser();
        List<FacebookPost> posts = parser.parsePostsPage(getClass().getResourceAsStream("/facebook/first.html"));
        checkFirstPosts(parser, posts);
        posts = parser.parsePostsPage(getClass().getResourceAsStream("/facebook/first_unformatted.html"));
        checkFirstPosts(parser, posts);
    }

    private void checkFirstPosts(FacebookParser parser, List<FacebookPost> posts) {
        assertNotNull(parser.getNextPageUrl());
        assertNotNull(posts);
        assertEquals("/stories.php?aftercursorr=1434774652%3A1434774652%3A8%3A845699147856585156%3A1434720769%3A0%3A0%3A180&tab=h_nor&__m_log_async__=1&refid=8", parser.getNextPageUrl());
        assertEquals(8, posts.size());

        FacebookPost firstPost = posts.get(0);
        assertEquals("harrypottermovie", firstPost.getAuthorId());
        assertEquals("Harry Potter", firstPost.getAuthorName());
        assertEquals("Harry Potter shared Universal Orlando Resort's album.", firstPost.getHeader());
        assertEquals(FacebookPost.TYPE_SHARED, firstPost.parseType());
        assertEquals("<p><a href=\"/hashtag/tbt?refid=8&_ft_=qid.6162310208335459589%3Amf_story_key.-4428925480907251478\">\u202A#\u200ETBT\u202C</a>" +
                " to the opening of The Wizarding World of Harry Potter at " +
                "<a href=\"/UniversalOrlandoResort?refid=8&_ft_=qid.6162310208335459589%3Amf_story_key.-4428925480907251478\">Universal Orlando Resort</a>" +
                " in 2010.</p>", firstPost.content);
        assertNull(firstPost.photo);
        assertNull(firstPost.photosLink);
        assertEquals("Yesterday at 12:03pm", firstPost.createdAt);
        assertEquals("Public", firstPost.visibility);
        assertEquals(94426, firstPost.likesCount);
        assertEquals(2744, firstPost.commentsCount);

        FacebookPost sharedPost = firstPost.sharedPost;
        assertNotNull(sharedPost);
        assertEquals("UniversalOrlandoResort", sharedPost.authorId);
        assertEquals("Universal Orlando Resort", sharedPost.authorName);
    }

    @Test
    public void testFacebookSecondPageParse() throws Exception {
        FacebookParser parser = new FacebookParser();
        List<FacebookPost> posts = parser.parsePostsPage(getClass().getResourceAsStream("/facebook/second.html"));
        checkSecondPosts(parser, posts);
        posts = parser.parsePostsPage(getClass().getResourceAsStream("/facebook/second_unformatted.html"));
        checkSecondPosts(parser, posts);
    }

    private void checkSecondPosts(FacebookParser parser, List<FacebookPost> posts) {
        assertNotNull(posts);
        assertNotNull(parser.getNextPageUrl());
        assertEquals(8, posts.size());
        // the most important are these five fields:
        FacebookPost firstPost = posts.get(0);
        assertEquals("849541311762258", firstPost.id);
        assertEquals("androidauthority", firstPost.authorId);
        assertEquals("Android Authority", firstPost.authorName);
        assertEquals("<p>Samsung has just issued an official fix for the missing quick toggles bug on the Galaxy S6/S6 Edge.</p>", firstPost.content);
        assertEquals("Yesterday at 6:28am", firstPost.createdAt);

        // check other fields here
        assertEquals("Public", firstPost.visibility);
        assertEquals(1308, firstPost.likesCount);
        assertEquals(35, firstPost.commentsCount);
    }

    @Test
    public void testFacebookThirdPageParse() throws Exception {
        FacebookParser parser = new FacebookParser();
        List<FacebookPost> posts = parser.parsePostsPage(getClass().getResourceAsStream("/facebook/third.html"));
        checkThirdPosts(parser, posts);
        posts = parser.parsePostsPage(getClass().getResourceAsStream("/facebook/third_unformatted.html"));
        checkThirdPosts(parser, posts);
    }

    private void checkThirdPosts(FacebookParser parser, List<FacebookPost> posts) {
        assertNotNull(posts);
        assertNotNull(parser.getNextPageUrl());
        assertEquals(8, posts.size());
    }

    @Test
    public void testFacebookLikesPagesParse() throws Exception {
        FacebookParser parser = new FacebookParser();
        List<FacebookPost> posts = parser.parsePostsPage(getClass().getResourceAsStream("/facebook/r1.html"));
        assertEquals(8, posts.size());
        for (FacebookPost post : posts) {
            if (post.parseType() == FacebookPost.TYPE_LIKED) {
                System.out.println("Liked post: " + post.likedPost.getContent());
            } else {
                System.out.println("Post: " + post.getContent());
            }
        }

        FacebookPost likedPost = posts.get(0);
        assertNotNull(likedPost.likedPost);
        assertEquals("Mark Zuckerberg liked this.", likedPost.getHeader());
        assertEquals("zuck", likedPost.getAuthorId());
        assertEquals("Mark Zuckerberg", likedPost.getAuthorName());
        assertEquals(FacebookPost.TYPE_LIKED, likedPost.parseType());

        FacebookPost actualPost = likedPost.likedPost;
        assertNotNull(actualPost);
        assertEquals("10155967430355177", actualPost.getId());
        assertEquals("sheryl", actualPost.getAuthorId());
        assertEquals("Sheryl Sandberg", actualPost.getAuthorName());
        assertEquals("Sheryl Sandberg with Jan Koum", actualPost.getHeader());
        assertEquals("<p>900 million people now use WhatsApp every month to stay in touch with family and friends around the world. Congratulations to <a href=\"/jan.koum?refid=8&_ft_=qid.6190987047926248132%3Amf_story_key.-4436091057771103291%3AeligibleForSeeFirstBumping.\">Jan Koum</a> and everyone at WhatsApp on reaching this incredible milestone – and for still finding time to make us all laugh. </p>",
                actualPost.getContent());
        assertEquals("Yesterday at 12:29pm", actualPost.getCreatedAt());
        assertEquals(8098, actualPost.getLikesCount());
        assertEquals(165, actualPost.getCommentsCount());
        assertEquals("Public", actualPost.visibility);

        FacebookPost normalPost = posts.get(2);
        assertNull(normalPost.likedPost);
        assertEquals("androidauthority", normalPost.getAuthorId());
        assertEquals("Android Authority", normalPost.getAuthorName());
        assertEquals(FacebookPost.TYPE_POST, normalPost.parseType());
        assertEquals("Android Authority", normalPost.getHeader());
        assertEquals("<p>Could a smartphone from an audio company prove compelling? We try to find out with the Marshall London.</p>", normalPost.getContent());
        assertEquals("Yesterday at 1:32pm", normalPost.getCreatedAt());
        assertEquals("Public", normalPost.visibility);
        assertEquals(1099, normalPost.getLikesCount());
        assertEquals(66, normalPost.getCommentsCount());

        FacebookPost secondPost = posts.get(1);
        assertEquals(FacebookPost.TYPE_LIKED, secondPost.parseType());
        assertEquals("Mark Zuckerberg liked this.", secondPost.getHeader());
        assertEquals("zuck", secondPost.getAuthorId());
        assertEquals("Mark Zuckerberg", secondPost.getAuthorName());
        FacebookPost originalSecondPost = secondPost.likedPost;
        assertEquals("eldsjal", originalSecondPost.getAuthorId());
        assertEquals("Daniel Ek", originalSecondPost.getAuthorName());

        // TODO check other fields
        assertEquals(FacebookPost.TYPE_SHARED, originalSecondPost.parseType());

        for (FacebookPost post : posts) {
            System.out.println("Post: " + post);
            switch (post.parseType()) {
                case FacebookPost.TYPE_LIKED:
                    post = post.likedPost;
                    break;
                case FacebookPost.TYPE_SHARED:
                    post = post.sharedPost;
                    break;
            }
            assertNotNull(post.getAuthorName());
            assertNotNull(post.getAuthorId());
            assertNotNull(post.getContent());
        }
    }

    @Test
    public void testReal2PagesParse() throws Exception {
        FacebookParser parser = new FacebookParser();
        List<FacebookPost> posts = parser.parsePostsPage(getClass().getResourceAsStream("/facebook/r2.html"));
        assertEquals(8, posts.size());

        FacebookPost first = posts.get(0);
        assertEquals("10102457551439011", first.getId());
    }

    @Test
    public void testReal3PageParse() throws Exception {
        FacebookParser parser = new FacebookParser();
        List<FacebookPost> posts = parser.parsePostsPage(getClass().getResourceAsStream("/facebook/r3.html"));
        assertEquals(8, posts.size());
        // first is a direct post
        assertEquals("zuck", posts.get(0).getAuthorId());
        // second is a direct post with two authors
        assertEquals("zuck", posts.get(1).getAuthorId());
        // third is a liked post
        assertEquals("zuck", posts.get(2).getAuthorId());
        assertEquals("imbbn", posts.get(2).getLikedPost().getAuthorId());
        // fourth is a liked post
        assertEquals("zuck", posts.get(3).getAuthorId());
        assertEquals("hilary.rosen", posts.get(3).getLikedPost().getAuthorId());
        System.out.println("post 4: " + posts.get(4));
        // fifth is a direct post
        assertEquals("androidauthority", posts.get(4).getAuthorId());
        // sixth is a liked post
        assertEquals("zuck", posts.get(5).getAuthorId());
        assertEquals("abbydphillip", posts.get(5).getLikedPost().getAuthorId());
        // seventh is a direct post
        assertEquals("androidauthority", posts.get(6).getAuthorId());
        // eights is a direct post
        assertEquals("androidauthority", posts.get(7).getAuthorId());
    }

    @Test
    public void testReal4PageParse() throws Exception {
        FacebookParser parser = new FacebookParser();
        List<FacebookPost> posts = parser.parsePostsPage(getClass().getResourceAsStream("/facebook/r4.html"));
        assertEquals(8, posts.size());
        // first is a direct post
        FacebookPost first = posts.get(0);
        assertEquals("androidauthority", first.getAuthorId());
        assertEquals("Android Authority", first.getAuthorName());
//        assertEquals("155608091155587", first.getId());
        assertEquals("2 mins", first.getCreatedAt());

        // second is a comment reply
        FacebookPost second = posts.get(1);
        assertEquals("zuck", second.getAuthorId());
        assertEquals("Mark Zuckerberg", second.getAuthorName());
        assertEquals(FacebookPost.TYPE_COMMENT_REPLY, second.parseType());
        // probably incorrect html on webpage for this type of event
//        assertEquals("5 hrs", second.getCreatedAt());

        FacebookPost repliedPost = second.getLikedPost();
        assertEquals("zuck", repliedPost.getAuthorId());
        assertEquals("Mark Zuckerberg", repliedPost.getAuthorName());
        assertEquals("14 hrs", repliedPost.getCreatedAt());
        assertEquals("<p>I want to thank you all for your heartwarming congratulations on Max's birth and on starting the " +
                     "<a href=\"/chanzuckerberginitiative/?refid=8&_ft_=qid.6224405122984957031%3Amf_story_key.-5229028388431761426\">Chan Zuckerberg Initiative</a>. " +
                     "This whole community has been so loving and supportive.</p>" +
                     "<p> If you're interested in following the philanthropy work we're doing with the Chan Zuckerberg Initiative, I encourage you to like the page here:</p>" +
                     "<p> <a href=\"https://www.facebook.com/chanzuckerberginitiative?refid=8&_ft_=qid.6224405122984957031%3Amf_story_key.-5229028388431761426\">https://www.facebook.com/chanzuckerberginitiative</a></p>" +
                     "<p> Since we announced this a couple days ago, many people have asked about what we're planning to focus on and how we're...</p> " +
                     "<a href=\"/story.php?story_fbid=10102507695055801&id=4&refid=8&_ft_=qid.6224405122984957031%3Amf_story_key.-5229028388431761426&__tn__=%2As\">More</a>",
                     repliedPost.getContent());
        assertEquals(168636, repliedPost.getLikesCount());
        assertEquals(9035, repliedPost.getCommentsCount());
    }

    @Test
    public void testFacebookStreamPageParse() throws Exception {
        FacebookParser parser = new FacebookParser();
        List<FacebookPost> posts = parser.parsePostsPage(getClass().getResourceAsStream("/facebook/stream1.html"));
        assertNotNull(posts);
        assertFalse(posts.isEmpty());
    }

    @Test
    public void testFacebookAuthorsParse() throws Exception {
        FacebookParser parser = new FacebookParser();
        FacebookAuthor author;

        author = parser.parseAuthorPage(getClass().getResourceAsStream("/facebook/author/zuck.html"));
        assertNotNull(author);
        assertEquals("https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtf1/v/t1.0-1/12208495_10102454385528521_4749095086285673716_n.jpg?efg=eyJpIjoiYiJ9&oh=0d62e0120b4bf9cedced5c25d92f0570&oe=56AC8FEC&__gda__=1458929583_b6546bb5950d3866752acfedaa57354c", author.getImage());
        assertEquals("zuck", author.getNetworkId());
        assertEquals("Mark Zuckerberg", author.getName());

        author = parser.parseAuthorPage(getClass().getResourceAsStream("/facebook/author/androidauthority.html"));
        assertNotNull(author);
        assertEquals("https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpt1/v/t1.0-1/p480x480/12141602_896928517023537_5037502164886405476_n.png.jpg?efg=eyJpIjoiYiJ9&oh=4095276ab9fc84b890e6678d9e68262b&oe=56E495F9&__gda__=1457847880_a2a24871651a5cf76b371afcd3a50480", author.getImage());
        assertEquals("androidauthority", author.getNetworkId());
        assertEquals("Android Authority", author.getName());

        author = parser.parseAuthorPage(getClass().getResourceAsStream("/facebook/author/gary.html"));
        assertNotNull(author);
        assertEquals("https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtp1/t31.0-1/p720x720/10682201_586992694736115_7065368003927899491_o.jpg?efg=eyJpIjoiYiJ9", author.getImage());
        assertEquals("gary.johnson.315", author.getNetworkId());
        assertEquals("Gary Johnson", author.getName());

        author = parser.parseAuthorPage(getClass().getResourceAsStream("/facebook/author/kazi.html"));
        assertNotNull(author);
        assertEquals("https://fbcdn-profile-a.akamaihd.net/hprofile-ak-prn2/v/t1.0-1/p720x720/10806330_10152871009297867_5789231808089176291_n.jpg?efg=eyJpIjoiYiJ9&oh=bbd936c29016e9ad6b913f1d838ef5d4&oe=56E80F0F&__gda__=1458461413_18aea4a4a1b4827b82504c7659884881", author.getImage());
        assertEquals("kazi", author.getNetworkId());
        assertEquals("Rousseau Kazi", author.getName());
    }
}
