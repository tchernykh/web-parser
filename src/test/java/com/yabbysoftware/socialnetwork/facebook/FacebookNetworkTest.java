package com.yabbysoftware.socialnetwork.facebook;

import com.yabbysoftware.network.SerializableCookie;
import org.junit.Test;

import java.io.*;
import java.net.HttpCookie;
import java.util.*;

import static org.junit.Assert.*;

/**
 * Test for facebook loading
 */
public class FacebookNetworkTest {
    private Properties props;

    public String getProperty(String name) throws Exception {
        String res = System.getProperty(name);
        if (res == null) {
            if (props == null) {
                props = new Properties();
                props.load(FacebookNetworkTest.class.getClassLoader().getResourceAsStream("test.properties"));
            }
            res = props.getProperty(name);
        }
        return res;
    }

    @Test
    public void testLoadingData() throws Exception {
        if (Boolean.valueOf(getProperty("networkTests"))) {
            FacebookNetwork facebookNetwork = new FacebookNetwork();
            FacebookCredentials facebookCredentials = new FacebookCredentials();
            facebookCredentials.setUsername(getProperty("facebookUsername"));
            facebookCredentials.setPassword(getProperty("facebookPassword"));
            facebookNetwork.login(facebookCredentials);

            List<FacebookPost> firstPage = new ArrayList<>(facebookNetwork.loadLatestPosts());
            System.out.println(facebookNetwork.getNextPageUrl());
            Set<String> authors = new HashSet<>();
            for (FacebookPost facebookPost : firstPage) {
                authors.add(facebookPost.authorId);
            }
            firstPage = new ArrayList<>(facebookNetwork.loadLatestPosts());
            System.out.println(facebookNetwork.getNextPageUrl());
            for (FacebookPost facebookPost : firstPage) {
                authors.add(facebookPost.authorId);
            }
            for (String authorId : authors) {
                FacebookAuthor author = facebookNetwork.loadAuthor(authorId);
                System.out.println("author " + author);
            }
        }
    }

    @Test
    public void testLoading() throws Exception {
        if (Boolean.valueOf(getProperty("networkTests"))) {
            FacebookNetwork facebookNetwork = new FacebookNetwork();
            FacebookCredentials facebookCredentials = new FacebookCredentials();
            facebookCredentials.setUsername(getProperty("facebookUsername"));
            facebookCredentials.setPassword(getProperty("facebookPassword"));
            facebookNetwork.login(facebookCredentials);

            loadAndAssertFacebookPosts(facebookNetwork);
        }
    }

    public static void loadAndAssertFacebookPosts(FacebookNetwork facebookNetwork) throws Exception {
        List<FacebookPost> firstPage = new ArrayList<>(facebookNetwork.loadLatestPosts());
        assertNotNull(facebookNetwork.getNextPageUrl());
        assertNotNull(firstPage);
        assertTrue(firstPage.size() > 0);
        for (FacebookPost post : firstPage) {
            assertNotNull(post.id);
            assertNotNull(post.authorId);
            assertNotNull(post.authorName);
        }

        List<FacebookPost> secondPage = new ArrayList<>(facebookNetwork.loadLatestPosts());
        assertNotNull(facebookNetwork.getNextPageUrl());
        assertNotNull(secondPage);
        assertTrue(secondPage.size() > 0);
        for (FacebookPost post : secondPage) {
            assertNotNull(post.id);
            assertNotNull(post.authorId);
            assertNotNull(post.authorName);
        }

        FacebookPost postFromFirstPage = firstPage.get(0);
        FacebookPost postFromSecondPage = secondPage.get(0);
        assertNotEquals(postFromFirstPage.id, postFromSecondPage.id);
        assertNotEquals(postFromFirstPage.content, postFromSecondPage.content);
    }

    @Test
    public void testSavingStoredCookies() throws Exception {
        if (Boolean.valueOf(getProperty("networkTests"))) {
            FacebookNetwork facebookNetwork = new FacebookNetwork();
            FacebookCredentials facebookCredentials = new FacebookCredentials();
            facebookCredentials.setUsername(getProperty("facebookUsername"));
            facebookCredentials.setPassword(getProperty("facebookPassword"));
            List<HttpCookie> cookies = facebookNetwork.login(facebookCredentials);

            String fileAddr = getProperty("facebookCookiesFile");
            File cookiesFile = new File(fileAddr);
            if (cookiesFile.exists()) {
                cookiesFile.delete();
            }
            FileOutputStream fout = new FileOutputStream(cookiesFile);
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(SerializableCookie.toSerializable(cookies));
            oos.close();
        }
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testLoadingUsingStoredCookies() throws Exception {
        if (Boolean.valueOf(getProperty("networkTests"))) {
            try (FileInputStream fin = new FileInputStream(getProperty("facebookCookiesFile"))) {
                ObjectInputStream ois = new ObjectInputStream(fin);
                List<SerializableCookie> serializableCookies = (List<SerializableCookie>) ois.readObject();

                FacebookNetwork gPlusNetwork = new FacebookNetwork();
                gPlusNetwork.login(SerializableCookie.fromSerializable(serializableCookies));
                loadAndAssertFacebookPosts(gPlusNetwork);
            }
        }
    }
}
