package com.yabbysoftware.socialnetwork;

import com.squareup.okhttp.OkHttpClient;
import com.yabbysoftware.network.SerializableCookie;
import com.yabbysoftware.socialnetwork.facebook.FacebookCredentials;
import com.yabbysoftware.socialnetwork.facebook.FacebookNetwork;
import com.yabbysoftware.socialnetwork.facebook.FacebookPost;
import com.yabbysoftware.socialnetwork.googleplus.GPlusCredentials;
import com.yabbysoftware.socialnetwork.googleplus.GPlusNetwork;
import com.yabbysoftware.socialnetwork.googleplus.GPlusPost;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.net.CookieManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Check that all networks could share HttpClient
 */
public class NetworkTest {

    private Properties props;

    public String getProperty(String name) throws Exception {
        String res = System.getProperty(name);
        if (res == null) {
            if (props == null) {
                props = new Properties();
                props.load(getClass().getClassLoader().getResourceAsStream("test.properties"));
            }
            res = props.getProperty(name);
        }
        return res;
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testLoadingUsingCredentials() throws Exception {
        if (Boolean.valueOf(getProperty("networkTests"))) {
            OkHttpClient client = new OkHttpClient();
            CookieManager cHandler = new CookieManager();
            GPlusCredentials gPlusCredentials = new GPlusCredentials();
            gPlusCredentials.setUsername(getProperty("gPlusUsername"));
            gPlusCredentials.setPassword(getProperty("gPlusPassword"));
            GPlusNetwork gPlusNetwork = new GPlusNetwork(client, cHandler);
            gPlusNetwork.login(gPlusCredentials);
            loadAndAssertGplusPosts(gPlusNetwork);

            FacebookNetwork facebookNetwork = new FacebookNetwork(client, cHandler);
            FacebookCredentials facebookCredentials = new FacebookCredentials();
            facebookCredentials.setUsername(getProperty("facebookUsername"));
            facebookCredentials.setPassword(getProperty("facebookPassword"));
            facebookNetwork.login(facebookCredentials);
            loadAndAssertFacebookPosts(facebookNetwork);
        }
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testLoadingUsingStoredCookies() throws Exception {
        if (Boolean.valueOf(getProperty("networkTests"))) {
            OkHttpClient client = new OkHttpClient();
            CookieManager cHandler = new CookieManager();
            try (FileInputStream fin = new FileInputStream(getProperty("gPlusCookiesFile"))) {
                ObjectInputStream ois = new ObjectInputStream(fin);
                List<SerializableCookie> serializableCookies = (List<SerializableCookie>) ois.readObject();

                GPlusNetwork gPlusNetwork = new GPlusNetwork(client, cHandler);
                gPlusNetwork.login(SerializableCookie.fromSerializable(serializableCookies));
                loadAndAssertGplusPosts(gPlusNetwork);
            }

            try (FileInputStream fin = new FileInputStream(getProperty("facebookCookiesFile"))) {
                ObjectInputStream ois = new ObjectInputStream(fin);
                List<SerializableCookie> serializableCookies = (List<SerializableCookie>) ois.readObject();

                FacebookNetwork gPlusNetwork = new FacebookNetwork(client, cHandler);
                gPlusNetwork.login(SerializableCookie.fromSerializable(serializableCookies));
                loadAndAssertFacebookPosts(gPlusNetwork);
            }
        }
    }

    public static void loadAndAssertGplusPosts(GPlusNetwork gPlusNetwork) throws Exception {
        List<GPlusPost> firstPage = new ArrayList<>(gPlusNetwork.loadLatestPosts());
        assertNotNull(firstPage);
        assertTrue(firstPage.size() > 0);
        for (GPlusPost post : firstPage) {
            System.out.println("GPlusPost: " + post);
            assertNotNull(post.getId());
            assertNotNull(post.getAuthorId());
            assertNotNull(post.getAuthorName());
            assertNotNull(post.getCreatedAt());
        }
    }

    public static void loadAndAssertFacebookPosts(FacebookNetwork gPlusNetwork) throws Exception {
        List<FacebookPost> firstPage = new ArrayList<>(gPlusNetwork.loadLatestPosts());
        assertNotNull(firstPage);
        assertTrue(firstPage.size() > 0);
        for (FacebookPost post : firstPage) {
            System.out.println("FacebookPost: " + post);
            assertNotNull(post.getId());
            assertNotNull(post.getAuthorId());
            assertNotNull(post.getAuthorName());
        }
    }
}
