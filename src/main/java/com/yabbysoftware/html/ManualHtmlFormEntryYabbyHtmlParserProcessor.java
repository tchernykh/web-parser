package com.yabbysoftware.html;

import com.yabby.htmlparser.actions.*;
import com.yabby.htmlparser.annotation.*;
import com.yabby.htmlparser.impl.*;
import com.yabby.htmlparser.index.*;
import com.yabby.htmlparser.processor.*;
import com.yabby.htmlparser.rule.*;
import com.yabby.htmlparser.tools.*;
import com.yabby.htmlparser.*;
import java.util.*;

public class ManualHtmlFormEntryYabbyHtmlParserProcessor implements ObjectYabbyHtmlParserProcessor<HtmlFormEntry>, IAction {
    private List<HtmlFormEntry> data = new ArrayList<>();
    private List<List<Rule>> innerRulesStack = new ArrayList<>();
    private HtmlFormEntry currentItem;
    private List<Rule> rootRules;
    private int startNodeLevel = 0;

    private List<String> unprocessedFields = new ArrayList<>();
    {
        unprocessedFields.add("HtmlFormEntry");
        unprocessedFields.add("type");
        unprocessedFields.add("id");
        unprocessedFields.add("value");
    }
    // */input
    private Rule rootRule = new Rule(this,new AnyNode(), new TagRuleItem("input"));

    public ManualHtmlFormEntryYabbyHtmlParserProcessor() {
        rootRules = new ArrayList<>();
        rootRules.add(rootRule);
    }

    @Override
    public List<Rule> getActiveRules() {
        if (currentItem == null) {
            return rootRules;
        } else {
            return new ArrayList<>(innerRulesStack.get(innerRulesStack.size() - 1));
        }
    }

    @Override
    public int getStartNodeLevel() {
        return startNodeLevel;
    }

    @Override
    public void action(HtmlNode node, int level, Rule rule) {
        if (rule == rootRule) {
            currentItem = new HtmlFormEntry();
            startNodeLevel = level;
            data.add(currentItem);
            initInnerRules(node);
        }
    }

    private void initInnerRules(HtmlNode node) {
        currentItem.type = node.getAttr("type");
        currentItem.id = node.getAttr("id");
        currentItem.value = node.getAttr("value");
        ArrayList<Rule> innerRules = new ArrayList<>();
        innerRulesStack.add(innerRules);
    }

    @Override
    public void out(HtmlNode node, int level, Rule rule) {
        if (rule == rootRule) {
            currentItem = null;
            startNodeLevel = 0;
        }
    }
    @Override
    public List<HtmlFormEntry> getResult() {
        return data;
    }


    public List<String> getUnprocessedRules() {
        return unprocessedFields;
    }
}