package com.yabbysoftware.html;

import com.yabby.htmlparser.annotation.ARule;

/**
 */
@ARule("*/input")
public class HtmlFormEntry {
    @ARule(".type")
    public String type;
    @ARule(".id")
    public String id;
    @ARule(".name")
    public String name;
    @ARule(".value")
    public String value;

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
