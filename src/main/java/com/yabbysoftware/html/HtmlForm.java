package com.yabbysoftware.html;

import com.yabby.htmlparser.annotation.ARule;

import java.util.ArrayList;

/**
 */
@ARule("*/form")
public class HtmlForm {
    @ARule(".id")
    String id;
    @ARule(".action")
    String action;
    @ARule(".method")
    String method;
    @ARule(".class")
    String classStyle;
    @ARule("*/input")
    ArrayList<HtmlFormEntry> values;

    public String getId() {
        return id;
    }

    public String getAction() {
        return action;
    }

    public String getMethod() {
        return method;
    }

    public String getClassStyle() {
        return classStyle;
    }

    public ArrayList<HtmlFormEntry> getValues() {
        return values;
    }
}
