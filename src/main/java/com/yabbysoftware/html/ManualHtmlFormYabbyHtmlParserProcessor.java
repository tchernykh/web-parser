package com.yabbysoftware.html;

import com.yabby.htmlparser.annotation.*;
import com.yabby.htmlparser.impl.*;
import com.yabby.htmlparser.processor.*;
import com.yabby.htmlparser.rule.*;
import com.yabby.htmlparser.*;
import java.util.*;

public class ManualHtmlFormYabbyHtmlParserProcessor implements ObjectYabbyHtmlParserProcessor<HtmlForm>, IAction {
    private List<HtmlForm> data = new ArrayList<>();
    private List<List<Rule>> innerRulesStack = new ArrayList<>();
    private HtmlForm currentItem;
    private List<Rule> rootRules;
    private int startNodeLevel = 0;

    private ManualHtmlFormEntryYabbyHtmlParserProcessor valuesProcessor = null;
    private List<String> unprocessedFields = new ArrayList<>();
    {
        unprocessedFields.add("HtmlForm");
        unprocessedFields.add("action");
        unprocessedFields.add("method");
        unprocessedFields.add("values");
    }
    // */form(class='mobile-login-form')
    private Rule rootRule = new Rule(this,new AnyNode(), new TagWithAttributeRuleItem("form", "class", "mobile-login-form"));
    // */input
    private Rule valuesRule = new Rule(this, new AnyNode(), new TagRuleItem("input"));

    public ManualHtmlFormYabbyHtmlParserProcessor() {
        rootRules = new ArrayList<>();
        rootRules.add(rootRule);
    }

    @Override
    public List<Rule> getActiveRules() {
        if (valuesProcessor != null) {
            return valuesProcessor.getActiveRules();
        } else if (currentItem == null) {
            return rootRules;
        } else {
            return new ArrayList<>(innerRulesStack.get(innerRulesStack.size() - 1));
        }
    }

    @Override
    public int getStartNodeLevel() {
        if (valuesProcessor != null) {
            return valuesProcessor.getStartNodeLevel();
        } else return startNodeLevel;
    }

    @Override
    public void action(HtmlNode node, int level, Rule rule) {
        if (rule == rootRule) {
            currentItem = new HtmlForm();
            startNodeLevel = level;
            data.add(currentItem);
            initInnerRules(node);
        } else if (rule == valuesRule) {
            unprocessedFields.remove("values");
            valuesProcessor = new ManualHtmlFormEntryYabbyHtmlParserProcessor();
            Rule rootRule = valuesProcessor.getActiveRules().get(0);
            valuesProcessor.action(node, level, rootRule);
        }
    }

    private void initInnerRules(HtmlNode node) {
        currentItem.action = node.getAttr("action");
        currentItem.method = node.getAttr("method");
        ArrayList<Rule> innerRules = new ArrayList<>();
        innerRules.add(valuesRule);
        innerRulesStack.add(innerRules);
    }

    @Override
    public void out(HtmlNode node, int level, Rule rule) {
        if (rule == rootRule) {
            currentItem = null;
            startNodeLevel = 0;
        } else if (rule == valuesRule) {
            if (currentItem.values == null) currentItem.values = new ArrayList<>();
            currentItem.values.addAll(valuesProcessor.getResult());
            valuesProcessor = null;
        }
    }
    @Override
    public List<HtmlForm> getResult() {
        return data;
    }


    public List<String> getUnprocessedRules() {
        return unprocessedFields;
    }
}