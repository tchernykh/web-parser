package com.yabbysoftware.socialnetwork.facebook;

import com.yabbysoftware.socialnetwork.ISocialNetworkCredentials;

/**
 * Credentials to log in to facebook
 */
public class FacebookCredentials implements ISocialNetworkCredentials {
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
