package com.yabbysoftware.socialnetwork.facebook;

import com.squareup.okhttp.OkHttpClient;
import com.yabbysoftware.socialnetwork.ISocialNetwork;
import com.yabbysoftware.socialnetwork.tools.Utils;

import java.io.InputStream;
import java.net.CookieManager;
import java.net.HttpCookie;
import java.util.List;
import java.util.Map;

/**
 * Network for facebook
 */
public class FacebookNetwork implements ISocialNetwork<FacebookPost, FacebookAuthor, FacebookCredentials> {

    private FacebookLoader loader;
    private FacebookParser parser;
    private boolean loggedIn = false;

    public FacebookLoader getLoader() {
        if (loader == null) {
            loader = new FacebookLoader();
        }
        return loader;
    }

    public FacebookParser getParser() {
        if (parser == null) {
            parser = new FacebookParser();
        }
        return parser;
    }

    public String getNextPageUrl() {
        return getParser().getNextPageUrl();
    }

    public FacebookNetwork() {

    }

    public FacebookNetwork(OkHttpClient client, CookieManager cHandler) {
        loader = new FacebookLoader(client, cHandler);
    }

    @Override
    public boolean isLoggedin() {
        return loggedIn;
    }

    @Override
    public List<HttpCookie> login(FacebookCredentials credentials) throws Exception {
        InputStream loginPageStream = getLoader().loadLoginPage();
        Map<String, String> loginPageParameters = getParser().parseLoginPage(loginPageStream);
        loginPageParameters.put("email", credentials.getUsername());
        loginPageParameters.put("pass", credentials.getPassword());
        Utils.safeClose(getLoader().login(loginPageParameters));
        loggedIn = true;
        return getLoader().getCookies();
    }

    @Override
    public void login(List<HttpCookie> cookies) throws Exception {
        getLoader().setCookies(cookies);
        loggedIn = true;
    }

    @Override
    public List<FacebookPost> loadLatestPosts() throws Exception {
        FacebookLoader loader = getLoader();
        if (!isLoggedin()) {
            throw new IllegalStateException("Not logged in");
        }
        return getParser().parsePostsPage(loader.loadPostsPage(getParser().getNextPageUrl()));

    }

    @Override
    public FacebookAuthor loadAuthor(String authorId) throws Exception {
        FacebookLoader loader = getLoader();
        if (!isLoggedin()) {
            throw new IllegalStateException("Not logged in");
        }
        return getParser().parseAuthorPage(loader.loadAuthorPage(authorId));
    }

    @Override
    public void addPost(FacebookPost post) {
    }
}
