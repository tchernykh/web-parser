package com.yabbysoftware.socialnetwork.facebook;

import com.yabby.htmlparser.annotation.ARule;
import com.yabbysoftware.socialnetwork.IAuthor;

/**
 * Author for facebook
 */
@ARule("*/div(id='root')")
public class FacebookAuthor implements IAuthor {
    @ARule("*/a(contains(href, 'v=timeline')).href.between('/','?')")
    String networkId;
    @ARule("[1][1]/*/img(contains(src, 'fbcdn-profile')).alt.after('Profile picture of ')")
    String name;
    @ARule("[1][1]/*/img(contains(src, 'fbcdn-profile')).src")
    String image;

    public String getNetworkId() {
        return networkId;
    }

    public void setNetworkId(String networkId) {
        this.networkId = networkId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
