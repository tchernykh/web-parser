package com.yabbysoftware.socialnetwork.facebook;

import com.yabby.htmlparser.annotation.ARule;
import com.yabbysoftware.socialnetwork.IPost;

/**
 * Facebook post
 */
@ARule("*/div(id='viewport')/*/div(id='m_newsfeed_stream')/div(id='m-top-of-feed')[^1]/div(id)")
public class FacebookPost implements IPost {

    public static final int TYPE_POST = 0;
    public static final int TYPE_LIKED = 1;
    public static final int TYPE_SHARED = 2;
    public static final int TYPE_COMMENT_REPLY = 3;

    @ARule("div/{div}/h3(class).plainText()") String header;

    @ARule("[2]/div[^1]/*/a(aria-label='Likes').href.between('story_fbid=','&')") String id;
    String publicity;

    // Header
    /**
     * Could have one of following paths:
     * /div/div/h3/a
     * /div/h3/strong/a
     * /div/div/h3/strong/a
     * /div/div/table/tbody/tr/td/h3/strong/a
     * where could have both strong and no-strong at the same time and we must use the strong one
     * */
    @ARule("div/*/h3/*/strong/a") String authorName;
    @ARule("div/*/h3/*/strong/a.href.between('/','?')") String authorId;
    @ARule("div/*/h3/*/a") String additionalAuthorName;
    @ARule("div/*/h3/*/a.href.between('/','/?')") String additionalAuthorId;

    @ARule("[1]/*/div(class='dg')/span/p") String tag;
    @ARule("[1]/*/div(class='dh')/*/a.href") String photosLink;
    @ARule("[1]/*/div(class='dh')/*/a.href") String photo;

    // Content
    @ARule("[1][2]") String content;
    @ARule("[1][3]") String content2;

    // Footer
    @ARule("[2]/div/abbr") String createdAt;
    @ARule("[2]/div/abbr[^2]") String visibility;
    @ARule("[2]/div[^1]/*/a(aria-label='Likes')") int likesCount;
    @ARule("[2]/div[^1]/a(contains(text(), 'Comments'))") int commentsCount;

    @ARule("div(id)") FacebookPost likedPost;
    @ARule("*/div/div(id)") FacebookPost sharedPost;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int parseType() {
        if (header == null || header.isEmpty()) return TYPE_POST;
        if (header.contains("liked")) return TYPE_LIKED;
        if (header.contains("shared")) return TYPE_SHARED;
        if (header.contains("replied")) return TYPE_COMMENT_REPLY;
        return TYPE_POST;
    }

    public String getAuthorName() {
        return authorName == null? additionalAuthorName: authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorId() {
        return authorId == null? additionalAuthorId: authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getPhotosLink() {
        return photosLink;
    }

    public void setPhotosLink(String photosLink) {
        this.photosLink = photosLink;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent2() {
        return content2;
    }

    public void setContent2(String content2) {
        this.content2 = content2;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    public FacebookPost getSharedPost() {
        return sharedPost;
    }

    public void setSharedPost(FacebookPost sharedPost) {
        this.sharedPost = sharedPost;
    }

    public String getPublicity() {
        return publicity;
    }

    public void setPublicity(String publicity) {
        this.publicity = publicity;
    }

    public FacebookPost getLikedPost() {
        return likedPost;
    }

    public void setLikedPost(FacebookPost likedPost) {
        this.likedPost = likedPost;
    }

    @Override
    public String toString() {
        return "FacebookPost{" +
                "id='" + id + '\'' +
                ", header='" + header + '\'' +
                ", authorName='" + authorName + '\'' +
                ", authorId='" + authorId + '\'' +
                ", tag='" + tag + '\'' +
                ", photosLink='" + photosLink + '\'' +
                ", photo='" + photo + '\'' +
                ", content='" + content + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", visibility='" + visibility + '\'' +
                ", likesCount=" + likesCount +
                ", commentsCount=" + commentsCount +
                ", sharedPost=" + sharedPost +
                ", likedPost=" + likedPost +
                '}';
    }
}
