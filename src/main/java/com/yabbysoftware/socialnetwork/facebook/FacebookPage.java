package com.yabbysoftware.socialnetwork.facebook;

import com.yabby.htmlparser.annotation.ARule;

import java.util.List;

/**
 *
 */
@ARule("*/div(id='viewport')/*/div(id='root')")
public class FacebookPage {
    @ARule("*/div(id='m-top-of-feed')[^1]/div(id)") List<FacebookPost> posts;
    @ARule("*/div(id='m-top-of-feed')[^2]/a.href") String nextPageUrl;
}
