package com.yabbysoftware.socialnetwork.facebook;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;
import com.yabbysoftware.socialnetwork.WebsiteLoader;

import java.io.IOException;
import java.io.InputStream;
import java.net.CookieManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Load facebook data - pages and resources
 */
public class FacebookLoader extends WebsiteLoader {
    public static final String FACEBOOK_URL = "https://m.facebook.com";
    public static final String FACEBOOK_AUTH_URL = "https://m.facebook.com/login.php";

    public FacebookLoader(OkHttpClient client, CookieManager cHandler) {
        super(client, cHandler);
    }

    public FacebookLoader() {
    }

    @Override
    protected List<Interceptor> getInterceptors() {
        List<Interceptor> interceptors = new ArrayList<>();
        interceptors.add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                return chain.proceed(chain.request().newBuilder()
                        .header("Host", chain.request().httpUrl().host())
                        .build());
            }
        });
        return interceptors;
    }

    @Override
    protected String getReferer() {
        return "m.facebook.com";
    }

    public InputStream loadLoginPage() throws Exception {
        return get(FACEBOOK_URL);
    }

    public InputStream login(Map<String,String> loginParameters) throws Exception {
        return post(FACEBOOK_AUTH_URL, loginParameters);
    }

    public InputStream loadPostsPage(String nextPageUrl) throws Exception {
        return get(FACEBOOK_URL + (nextPageUrl == null? "" : nextPageUrl));
    }

    public InputStream loadAuthorPage(String authorId) throws Exception {
        return get(FACEBOOK_URL + "/" + authorId);
    }
}
