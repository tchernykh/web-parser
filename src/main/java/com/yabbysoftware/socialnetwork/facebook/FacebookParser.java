package com.yabbysoftware.socialnetwork.facebook;

import com.yabby.htmlparser.YabbyHtmlParser;
import com.yabby.htmlparser.impl.ParserSettings;
import com.yabby.htmlparser.impl.jsoup.JsoupParser;
import com.yabbysoftware.html.HtmlForm;
import com.yabbysoftware.html.HtmlFormEntry;
import com.yabbysoftware.socialnetwork.tools.Utils;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Parse facebook pages
 */
public class FacebookParser {

    private YabbyHtmlParser<FacebookPage> yabbyHtmlParser;
    private YabbyHtmlParser<FacebookAuthor> authorHtmlParser;
    private JsoupParser parser;
    private String nextPageUrl;

    public String getNextPageUrl() {
        return nextPageUrl;
    }

    public FacebookParser() {
        try {
            parser = new JsoupParser();
            ParserSettings parserSettings = new ParserSettings();
            parserSettings.setParsingType(ParserSettings.ParsingType.ANDROID);
            parser.setParserSettings(parserSettings);
            yabbyHtmlParser = new YabbyHtmlParser<>(parser, FacebookPage.class);
            authorHtmlParser = new YabbyHtmlParser<>(parser, FacebookAuthor.class);
//            yabbyHtmlParser.setDebug(true);
        } catch (Exception e) {
            // Shouldn't happen
            e.printStackTrace();
        }
    }

    public Map<String, String> parseLoginPage(InputStream pageStream) throws Exception {
        try {
            YabbyHtmlParser<HtmlForm> loginPageValues = new YabbyHtmlParser<>(parser, HtmlForm.class);
            List<HtmlForm> forms = loginPageValues.parse(pageStream);
            for (HtmlForm form : forms) {
                if (form.getClassStyle().contains("mobile-login-form")) {
                    List<HtmlFormEntry> list = form.getValues();
                    Map<String, String> formValues = new HashMap<>(list.size());
                    for (HtmlFormEntry htmlFormEntry : list) {
                        formValues.put(htmlFormEntry.getName(), htmlFormEntry.getValue());
                    }
                    return formValues;
                }
            }
        } finally {
            Utils.safeClose(pageStream);
        }
        return null;
    }

    public List<FacebookPost> parsePostsPage(InputStream pageStream) throws Exception {
        try {
            List<FacebookPage> page = yabbyHtmlParser.parse(pageStream);
            List<FacebookPost> facebookPosts = page.get(0).posts;
            nextPageUrl = page.get(0).nextPageUrl;
            Iterator<FacebookPost> iterator = facebookPosts.iterator();
            while (iterator.hasNext()) {
                FacebookPost next = iterator.next();
                // not valid post
                if (next.getId() == null && next.likedPost == null && next.sharedPost == null) {
                    iterator.remove();
                }
            }
            return facebookPosts;
        } finally {
            Utils.safeClose(pageStream);
        }
    }

    public FacebookAuthor parseAuthorPage(InputStream authorStream) throws Exception {
        try {
            List<FacebookAuthor> author = authorHtmlParser.parse(authorStream);
            return author.isEmpty() ? null : author.get(0);
        } finally {
            Utils.safeClose(authorStream);
        }
    }
}
