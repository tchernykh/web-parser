package com.yabbysoftware.socialnetwork.googleplus;

import com.yabby.htmlparser.annotation.ARule;
import com.yabbysoftware.socialnetwork.IAuthor;

/**
 * Author information from GooglePlus
 */
@ARule("*/div(id='PAGE');")
public class GPlusAuthor implements IAuthor {
    @ARule("div(class='dH')/*/div(class='z8').plainText()") String authorName;
    @ARule("*/div(class='ipc')/*/a.href") String authorWebsite;
    @ARule("*/div(class='QJwXXd')/*/div(class='apc')/a.href.between('/app/basic/photos/','/album/profile')") String authorId;
    @ARule("*/div(class='QJwXXd')/*/img(class='Zoc').src") String authorPhoto;
    @ARule("*/div(class='QJwXXd')/*/img(class='tpc').src") String authorCover;

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorWebsite() {
        return authorWebsite;
    }

    public void setAuthorWebsite(String authorWebsite) {
        this.authorWebsite = authorWebsite;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getAuthorPhoto() {
        return authorPhoto;
    }

    public void setAuthorPhoto(String authorPhoto) {
        this.authorPhoto = authorPhoto;
    }

    public String getAuthorCover() {
        return authorCover;
    }

    public void setAuthorCover(String authorCover) {
        this.authorCover = authorCover;
    }

    @Override
    public String toString() {
        return "#" + authorId + "\n" +
                authorName + ":" + authorWebsite + "\n" +
                authorPhoto + ":" + authorCover;
    }
}
