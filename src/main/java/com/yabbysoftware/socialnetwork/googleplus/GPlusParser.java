package com.yabbysoftware.socialnetwork.googleplus;

import com.yabby.htmlparser.YabbyHtmlParser;
import com.yabby.htmlparser.impl.ParserSettings;
import com.yabby.htmlparser.impl.TextProcessor;
import com.yabby.htmlparser.impl.jsoup.JsoupParser;
import com.yabbysoftware.html.HtmlForm;
import com.yabbysoftware.html.HtmlFormEntry;
import com.yabbysoftware.socialnetwork.tools.Utils;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GPlusParser {

    private static final char END_LINE_CHAR = 0xfeff;

    private YabbyHtmlParser<GPlusPage> yabbyHtmlParser;
    private YabbyHtmlParser<GPlusAuthor> yabbyHtmlAuthorParser;
    private JsoupParser parser;
    private String nextPageUrl;

    public JsoupParser getParser() {
        return parser;
    }

    public String getNextPageUrl() {
        return nextPageUrl;
    }

    public GPlusParser() {
        try {
            parser = new JsoupParser();
            ParserSettings parserSettings = new ParserSettings();
            parserSettings.setParsingType(ParserSettings.ParsingType.ANDROID);
            parserSettings.setTextProcessor(new TextProcessor() {
                @Override
                public StringBuilder processText(StringBuilder stringBuilder) {
                    if (stringBuilder.length() > 0 && stringBuilder.charAt(stringBuilder.length() - 1) == END_LINE_CHAR) {
                        stringBuilder.setLength(stringBuilder.length() - 1);
                    }
                    return stringBuilder;
                }

                @Override
                public String processValue(String name, String value) {
//                    if (!value.isEmpty() && value.charAt(value.length() - 1) == END_LINE_CHAR) {
//                        value = value.substring(0, value.length() - 1);
//                    }
                    return value;
                }
            });
            parser.setParserSettings(parserSettings);
            yabbyHtmlParser = new YabbyHtmlParser<>(parser, GPlusPage.class);
            yabbyHtmlAuthorParser = new YabbyHtmlParser<>(parser, GPlusAuthor.class);
        } catch (Exception e) {
            // Shouldn't happen
            e.printStackTrace();
        }
    }

    public List<GPlusPost> parsePostsPage(InputStream pageStream) throws Exception {
        try {
            GPlusPage page = yabbyHtmlParser.parse(pageStream).get(0);
            nextPageUrl = page.nextPageUrl;
            return page.posts;
        } finally {
            Utils.safeClose(pageStream);
        }
    }

    public Map<String, String> parseLoginPage(InputStream pageStream) throws Exception {
        try {
            YabbyHtmlParser<HtmlForm> loginPageValues = new YabbyHtmlParser<>(parser, HtmlForm.class);
            List<HtmlForm> forms = loginPageValues.parse(pageStream);
            for (HtmlForm form : forms) {
                if ("gaia_loginform".equals(form.getId())) {
                    List<HtmlFormEntry> list = form.getValues();
                    Map<String, String> formValues = new HashMap<>(list.size());
                    for (HtmlFormEntry htmlFormEntry : list) {
                        formValues.put(htmlFormEntry.getName(), htmlFormEntry.getValue());
                    }
                    return formValues;
                }
            }
        } finally {
            Utils.safeClose(pageStream);
        }
        return null;
    }

    public GPlusAuthor parseAuthorPage(InputStream pageStream) throws Exception {
        try {
            List<GPlusAuthor> parse = yabbyHtmlAuthorParser.parse(pageStream);
            return parse.isEmpty()? null: parse.get(0);
        } finally {
            Utils.safeClose(pageStream);
        }
    }
}
