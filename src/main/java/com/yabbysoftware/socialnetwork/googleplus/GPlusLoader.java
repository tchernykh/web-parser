package com.yabbysoftware.socialnetwork.googleplus;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;
import com.yabbysoftware.socialnetwork.WebsiteLoader;

import java.io.IOException;
import java.io.InputStream;
import java.net.CookieManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * GooglePlus stream loader using okhttp
 * TODO think about merging with GPlusNetwork
 */
public class GPlusLoader extends WebsiteLoader {

    private static final String GOOGLE_AUTH_URL = "https://accounts.google.com/ServiceLoginAuth";
    private static final String GOOGLE_PLUS_URL = "https://plus.google.com";
    private static final String GOOGLE_PLUS_AUTHOR_PREFIX_URL = "https://plus.google.com/app/basic/";
    private static final String GOOGLE_PLUS_AUTH_USER_AGENT = "Mozilla/4.0";
    private static final String GOOGLE_PLUS_USER_AGENT = "Mozilla/5.0 (BB10; Touch) AppleWebKit/537.10+ (KHTML, like Gecko) Version/10.0.9.2372 Mobile Safari/537.10+";

    private String userAgent = GOOGLE_PLUS_USER_AGENT;

    public GPlusLoader() {
    }

    public GPlusLoader(OkHttpClient client, CookieManager cHandler) {
        super(client, cHandler);
    }

    @Override
    protected String getReferer() {
        return GOOGLE_AUTH_URL;
    }

    @Override
    protected String getUserAgent() {
        return userAgent;
    }

    @Override
    protected List<Interceptor> getInterceptors() {
        List<Interceptor> interceptors = new ArrayList<>();
        interceptors.add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                if ("plus.google.com".equals(chain.request().httpUrl().host())) {
                    return chain.proceed(chain.request().newBuilder()
                            .header("Origin", "https://accounts.google.com")
                            .build());
                }
                return chain.proceed(chain.request());
            }
        });
        return interceptors;
    }

    public InputStream loadPostsPage(String url) throws Exception {
        userAgent = GOOGLE_PLUS_USER_AGENT;
        if (url == null) {
            url = GOOGLE_PLUS_URL;
        } else {
            url = GOOGLE_PLUS_URL + url;
        }
        return get(url);
    }

    public InputStream loadAuthorPage(String authorId) throws Exception {
        userAgent = GOOGLE_PLUS_USER_AGENT;
        return get(GOOGLE_PLUS_AUTHOR_PREFIX_URL + authorId + "/about");
    }

    public InputStream loadLoginPage() throws Exception {
        userAgent = GOOGLE_PLUS_AUTH_USER_AGENT;
        return get(GOOGLE_AUTH_URL);
    }

    public InputStream login(Map<String,String> loginParameters) throws Exception {
        userAgent = GOOGLE_PLUS_AUTH_USER_AGENT;
        return post(GOOGLE_AUTH_URL, loginParameters);
    }

    public InputStream loadImage(String imagePath, int width, int height) throws IOException {
        userAgent = GOOGLE_PLUS_USER_AGENT;
        imagePath = imagePath.replace("w1280", "w" + width);
        imagePath = imagePath.replace("h800", "h" + height);
        return get(imagePath);
    }

    public void verifyNumber(String formPostUrl, Map<String, String> formPostValues) throws Exception {
        userAgent = GOOGLE_PLUS_AUTH_USER_AGENT;
        post(formPostUrl, formPostValues);
    }
}