package com.yabbysoftware.socialnetwork.googleplus;

import com.yabbysoftware.socialnetwork.ISocialNetworkCredentials;

public class GPlusCredentials implements ISocialNetworkCredentials {
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
