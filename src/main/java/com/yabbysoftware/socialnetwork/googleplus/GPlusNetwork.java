package com.yabbysoftware.socialnetwork.googleplus;

import com.squareup.okhttp.OkHttpClient;
import com.yabbysoftware.socialnetwork.ISocialNetwork;
import com.yabbysoftware.socialnetwork.tools.Utils;

import java.io.InputStream;
import java.net.CookieManager;
import java.net.HttpCookie;
import java.util.List;
import java.util.Map;

public class GPlusNetwork implements ISocialNetwork<GPlusPost, GPlusAuthor, GPlusCredentials> {

    private GPlusLoader loader;
    private GPlusParser parser;
    private boolean loggedIn = false;

    public GPlusLoader getLoader() {
        if (loader == null) {
            loader = new GPlusLoader();
        }
        return loader;
    }

    public GPlusParser getParser() {
        if (parser == null) {
            parser = new GPlusParser();
        }
        return parser;
    }

    public GPlusNetwork() {

    }

    public GPlusNetwork(OkHttpClient client, CookieManager cHandler) {
        loader = new GPlusLoader(client, cHandler);
    }

    @Override
    public boolean isLoggedin() {
        return loggedIn;
    }

    @Override
    public List<HttpCookie> login(GPlusCredentials credentials) throws Exception {
        InputStream loginPageStream = getLoader().loadLoginPage();
        Map<String, String> loginPageParameters = getParser().parseLoginPage(loginPageStream);
        loginPageParameters.put("Email", credentials.getUsername());
        loginPageParameters.put("Passwd", credentials.getPassword());
        Utils.safeClose(getLoader().login(loginPageParameters));
        loggedIn = true;
        return getLoader().getCookies();
    }

    @Override
    public void login(List<HttpCookie> cookies) throws Exception {
        getLoader().setCookies(cookies);
        loggedIn = true;
    }

    @Override
    public List<GPlusPost> loadLatestPosts() throws Exception {
        GPlusLoader loader = getLoader();
        if (!isLoggedin()) {
            throw new IllegalStateException("Not logged in");
        }
        return getParser().parsePostsPage(loader.loadPostsPage(getParser().getNextPageUrl()));
    }

    @Override
    public GPlusAuthor loadAuthor(String authorId) throws Exception {
        GPlusLoader loader = getLoader();
        if (!isLoggedin()) {
            throw new IllegalStateException("Not logged in");
        }
        return getParser().parseAuthorPage(loader.loadAuthorPage(authorId));
    }

    @Override
    public void addPost(GPlusPost post) {

    }
}
