package com.yabbysoftware.socialnetwork.googleplus;

import com.yabby.htmlparser.annotation.ARule;
import com.yabbysoftware.socialnetwork.IPost;

/**
 * Google plus post model class
 */
@ARule("*/div(class='fHa')&div(data-itemid)")
public class GPlusPost implements IPost {
    // BASE FIELDS
    @ARule(".data-itemid")
    String id;
    @ARule("*/div(jsname='r4nke')/span(data-itemid)")
    String content;
    @ARule(value = "*/span(class='Sxa')")
    String createdAt;
    @ARule("*/a(class='Pqa')&a(id)")
    String authorName;
    @ARule("*/a(class='Pqa')&a(id).href.between('app/basic/','/posts')")
    String authorId;
    @ARule("*/div(class='HFM8ed')")
    String category;
    @ARule("*/a(class='Qxa').text().after('#')")
    String tag;

    // MEDIA CONTENT
    @ARule("*/iframe(class='qJXTfe').src")
    String mediaContentYoutube;
    @ARule("*/div(class='HzKZIe')/*/img.src")
    String mediaContentImage;
    String mediaContentMore;
    @ARule("*/div(class='lhk0af')/a.href")
    String mediaContentMoreLink;
    @ARule("*/div(class='HzKZIe')/a.href")
    String mediaContentLink;
    @ARule("*/div(class='HzKZIe')/*/div(class='M5e9Oe')")
    String mediaContentDescription;

    // TASK ACTIONS
    @ARule("*/div(class='taqksc')/*/button(class='c4QUad').data-count")
    int likesCount;
    @ARule("*/a(class='HKw7Ee')/*/div(class='QkiXVb')")
    int sharesCount;
    @ARule("*/a(class='AUdmse')/div(class='QkiXVb')")
    int commentsCount;

    // TOP COMMENT
    @ARule("*/div(class='EyJcc')/*/span(class='IGa')")
    String topCommentAuthor;
    @ARule("*/div(class='EyJcc')/*/span(class='gG2Vjf')")
    String topCommentDescription;

    // RESHARED POST DATA
    @ARule("*/div(class='uFb')/a(class='Pqa')&a(class='GXmiXb')")
    String resharedAuthorName;
    @ARule("*/div(class='uFb')/a(class='Pqa')&a(class='GXmiXb').href.between('app/basic/','/posts')")
    String resharedAuthorId;
    @ARule("*/div(jsname='vtn22d')")
    String reshareContent;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getMediaContentYoutube() {
        return mediaContentYoutube;
    }

    public void setMediaContentYoutube(String mediaContentYoutube) {
        this.mediaContentYoutube = mediaContentYoutube;
    }

    public String getMediaContentImage() {
        return mediaContentImage;
    }

    public void setMediaContentImage(String mediaContentImage) {
        this.mediaContentImage = mediaContentImage;
    }

    public String getMediaContentMore() {
        return mediaContentMore;
    }

    public void setMediaContentMore(String mediaContentMore) {
        this.mediaContentMore = mediaContentMore;
    }

    public String getMediaContentMoreLink() {
        return mediaContentMoreLink;
    }

    public void setMediaContentMoreLink(String mediaContentMoreLink) {
        this.mediaContentMoreLink = mediaContentMoreLink;
    }

    public String getMediaContentLink() {
        return mediaContentLink;
    }

    public void setMediaContentLink(String mediaContentLink) {
        this.mediaContentLink = mediaContentLink;
    }

    public String getMediaContentDescription() {
        return mediaContentDescription;
    }

    public void setMediaContentDescription(String mediaContentDescription) {
        this.mediaContentDescription = mediaContentDescription;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    public int getSharesCount() {
        return sharesCount;
    }

    public void setSharesCount(int sharesCount) {
        this.sharesCount = sharesCount;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    public String getTopCommentAuthor() {
        return topCommentAuthor;
    }

    public void setTopCommentAuthor(String topCommentAuthor) {
        this.topCommentAuthor = topCommentAuthor;
    }

    public String getTopCommentDescription() {
        return topCommentDescription;
    }

    public void setTopCommentDescription(String topCommentDescription) {
        this.topCommentDescription = topCommentDescription;
    }

    public String getResharedAuthorName() {
        return resharedAuthorName;
    }

    public void setResharedAuthorName(String resharedAuthorName) {
        this.resharedAuthorName = resharedAuthorName;
    }

    public String getResharedAuthorId() {
        return resharedAuthorId;
    }

    public void setResharedAuthorId(String resharedAuthorId) {
        this.resharedAuthorId = resharedAuthorId;
    }

    public String getReshareContent() {
        return reshareContent;
    }

    public void setReshareContent(String reshareContent) {
        this.reshareContent = reshareContent;
    }

    @Override
    public String toString() {
        return "GPlusPost{" +
                "id='" + id + '\'' +
                ", content='" + content + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", authorName='" + authorName + '\'' +
                ", authorId='" + authorId + '\'' +
                ", category='" + category + '\'' +
                ", tag='" + tag + '\'' +
                ", mediaContentYoutube='" + mediaContentYoutube + '\'' +
                ", mediaContentImage='" + mediaContentImage + '\'' +
                ", mediaContentMore='" + mediaContentMore + '\'' +
                ", mediaContentMoreLink='" + mediaContentMoreLink + '\'' +
                ", mediaContentLink='" + mediaContentLink + '\'' +
                ", mediaContentDescription='" + mediaContentDescription + '\'' +
                ", likesCount=" + likesCount +
                ", sharesCount=" + sharesCount +
                ", commentsCount=" + commentsCount +
                ", topCommentAuthor='" + topCommentAuthor + '\'' +
                ", topCommentDescription='" + topCommentDescription + '\'' +
                ", resharedAuthorName='" + resharedAuthorName + '\'' +
                ", resharedAuthorId='" + resharedAuthorId + '\'' +
                ", reshareContent='" + reshareContent + '\'' +
                '}';
    }
}

