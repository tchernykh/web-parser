package com.yabbysoftware.socialnetwork.googleplus;

import com.yabby.htmlparser.annotation.ARule;

import java.util.List;

/**
 */
@ARule("*/body/div(id='PAGE')")
public class GPlusPage {
    @ARule("*/div(class='kFb')/a.href") public String nextPageUrl;
    @ARule("*/div(class='fHa')&div(data-itemid)") public List<GPlusPost> posts;
}
