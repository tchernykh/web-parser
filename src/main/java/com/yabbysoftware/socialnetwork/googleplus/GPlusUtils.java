package com.yabbysoftware.socialnetwork.googleplus;

import java.util.Calendar;
import java.util.Date;

/**
 * Tool methods
 */
@SuppressWarnings("unused")
public final class GPlusUtils {
    private GPlusUtils() {
    }

    // TODO create junit test for this method
    public static Date toDate(String createdAt, Date fromDate) {
        if (createdAt == null || createdAt.length() == 0) return null;
        // we receive date in format like 10 minutes ago or 2 days ago
        String[] splittedDate = createdAt.split(" ");
        if (splittedDate.length < 2) return fromDate;
        int fieldType;
        if (splittedDate[1].startsWith("second")) {
            fieldType = Calendar.SECOND;
        } else if (splittedDate[1].startsWith("minute")) {
            fieldType = Calendar.MINUTE;
        } else if (splittedDate[1].startsWith("hour")) {
            fieldType = Calendar.HOUR_OF_DAY;
        } else if (splittedDate[1].startsWith("day")) {
            fieldType = Calendar.DATE;
        } else if (splittedDate[1].startsWith("week")) {
            fieldType = Calendar.WEEK_OF_YEAR;
        } else if (splittedDate[1].startsWith("month")) {
            fieldType = Calendar.MONTH;
        } else if (splittedDate[1].startsWith("year")) {
            fieldType = Calendar.YEAR;
        } else {
            // unknown type
            return fromDate;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(fromDate);
        c.add(fieldType, -Integer.parseInt(splittedDate[0]));
        return c.getTime();
    }

    public static String getImageUriWithWidth(String imageUri, int desiredWidth) {
        // there are two types of image urls:
        // https://lh5.googleusercontent.com/-_PAQsAbp1ds/VPoP2v3gUgI/AAAAAAAC9b4/fo97W5PyFf8/w1280-h800-p-k/androidlove.png
        // https://lh6.googleusercontent.com/proxy/tGdPUBjMLxnaB53fPoGiqLl8mq-qp5G0ckET2AdnLz5mz-pUFpHhymJDhV7piJT3mtcUIN6dxCzge1zmJHHytyTJ3P0=w1280-h800-o-k
        return imageUri.replace("w1280-h800-p-k", "s" + desiredWidth).replace("w1280-h800-o-k", "s" + desiredWidth);
    }
}
