package com.yabbysoftware.socialnetwork;

import java.net.HttpCookie;
import java.util.List;

/**
 * Implementations are social networks like Google+, LinkedIn, Vkontakte and Facebook
 */
public interface ISocialNetwork<P extends IPost, A extends IAuthor, T extends ISocialNetworkCredentials> {
    boolean isLoggedin();

    List<HttpCookie> login(T credentials) throws Exception;

    void login(List<HttpCookie> cookies) throws Exception;

    List<P> loadLatestPosts() throws Exception;

    A loadAuthor(String authorId) throws Exception;

    void addPost(P post);
}
