package com.yabbysoftware.socialnetwork.tools;

import java.io.*;

/**
 * Tools classes
 */
public final class Utils {

    public static void safeClose(InputStream stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
                // ignore exception
            }
        }
    }

    public static void safeClose(OutputStream stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
                // ignore exception
            }
        }
    }

    private static void safeClose(Reader reader) {
        if (reader != null) {
            try {
                reader.close();
            } catch (IOException e) {
                // ignore exception
            }
        }
    }

    public static String convertInputStreamToString(InputStream inputStream) throws IOException {
        final char[] buffer = new char[16];
        final StringBuilder out = new StringBuilder();
        try {
            final Reader in = new InputStreamReader(inputStream, "UTF-8");
            try {
                while (true) {
                    int rsz = in.read(buffer, 0, buffer.length);
                    if (rsz < 0)
                        break;
                    out.append(buffer, 0, rsz);
                }
            } finally {
                safeClose(in);
                safeClose(inputStream);
            }
        } catch (UnsupportedEncodingException ex) {
            // ignore
        }
        return out.toString();
    }
}
