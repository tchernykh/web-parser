package com.yabbysoftware.socialnetwork;

import com.squareup.okhttp.*;
import com.yabbysoftware.network.NetworkLogInterceptor;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Loader for a website
 */
public abstract class WebsiteLoader {

    private static final String DEFAULT_USER_AGENT = "Mozilla/5.0 (Linux; Android 4.4.4; en-us; Nexus 5Build/JOP40D) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2307.2 Mobile Safari/537.36";
    private static final String ACCEPT_HEADER_PARAM = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
    private static final String ACCEPT_LANGUAGE_HEADER_PARAM = "en-US,en;q=0.5";

    private OkHttpClient client;
    private CookieManager cHandler;

    public OkHttpClient getClient() {
        return client;
    }

    public WebsiteLoader(OkHttpClient client, CookieManager cHandler) {
        client.interceptors().addAll(getInterceptors());
        this.client = client;
        // make sure cookies is turn on
        this.cHandler = cHandler;
        CookieHandler.setDefault(cHandler);
    }

    public WebsiteLoader() {
        this(new OkHttpClient(), new CookieManager());
    }

    public void setInterceptedContentDirectory(File directory) {
        client.interceptors().add(0, new NetworkLogInterceptor(directory));
    }

    public void setCookies(List<HttpCookie> cookies) throws URISyntaxException {
        for (HttpCookie cookie : cookies) {
            cHandler.getCookieStore().add(new URI(cookie.getDomain()), cookie);
        }
    }

    public List<HttpCookie> getCookies() {
        return cHandler.getCookieStore().getCookies();
    }

    protected InputStream get(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .header("User-Agent", getUserAgent())
                .header("Accept", ACCEPT_HEADER_PARAM)
                .header("Accept-Language", ACCEPT_LANGUAGE_HEADER_PARAM)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().byteStream();
    }

    protected InputStream post(String url, Map<String, String> formPostValues) throws Exception {
        FormEncodingBuilder formBody = new FormEncodingBuilder();
        for (Map.Entry<String, String> entry : formPostValues.entrySet()) {
            String value = entry.getValue();
            if (value == null) {
                value = "";
            }
            formBody.add(entry.getKey(), value);
        }
        Request request = new Request.Builder()
                .url(url)
                .header("User-Agent", getUserAgent())
                .header("Accept", ACCEPT_HEADER_PARAM)
                .header("Accept-Language", ACCEPT_LANGUAGE_HEADER_PARAM)
                .header("Connection", "keep-alive")
                .header("Referer", getReferer())
                .header("Content-Type", "application/x-www-form-urlencoded")
                .post(formBody.build())
                .build();

        Response response = client.newCall(request).execute();
        return response.body().byteStream();
    }

    protected abstract String getReferer();

    protected String getUserAgent() {
        return DEFAULT_USER_AGENT;
    }

    protected List<Interceptor> getInterceptors() {
        return new ArrayList<>();
    }
}
