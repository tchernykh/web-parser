package com.yabbysoftware.network;

import java.util.List;
import java.util.Vector;

public final class NetworkPerformanceMeasureTools {

    private static final List<RequestInfo> measuredRequests = new Vector<>();

    private static boolean measuring;

    public static void startMeasure() {
        measuredRequests.clear();
        measuring = true;
    }

    public static List<RequestInfo> stopMeasure() {
        measuring = false;
        return measuredRequests;
    }

    public static void addRequestInfo(RequestInfo requestInfo) {
        if (measuring) {
            measuredRequests.add(requestInfo);
        }
    }

    public static class RequestInfo {
        public String uri;
        public long executionTime;
        public long sentBytesCount;
        public long receivedBytesCount;
    }
}
