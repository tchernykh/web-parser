package com.yabbysoftware.network;

import com.squareup.okhttp.*;
import com.squareup.okhttp.internal.http.RealResponseBody;
import com.yabbysoftware.socialnetwork.tools.Utils;
import okio.*;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class NetworkLogInterceptor implements Interceptor {

    private File interceptedContentDirectory;

    public NetworkLogInterceptor() {

    }

    public NetworkLogInterceptor(File interceptedContentDirectory) {
        this.interceptedContentDirectory = interceptedContentDirectory;
    }

    private void log(String message, Object... params) {
        System.out.println(String.format(message, params));
    }

    private void err(Throwable ex, String message, Object... params) {
        System.out.println("Error: " + String.format(message, params));
        ex.printStackTrace();
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originalRequest = chain.request();
        try {
            String entityString = null;
            if (originalRequest.body() != null) {
                BufferedSink sink = new Buffer();
                originalRequest.body().writeTo(sink);
                sink.flush();
                entityString = Utils.convertInputStreamToString(sink.buffer().inputStream());
            }
            StringBuilder sb = new StringBuilder();
            for (String headerName : originalRequest.headers().names()) {
                List<String> values = originalRequest.headers(headerName);
                for (String value : values) {
                    sb.append(headerName).append(":").append(value).append("\n");
                }
            }
            System.out.println();
            log("request %s %s body: %s \n headers: %s", originalRequest.method(),
                    originalRequest.urlString(), entityString, sb);
        }   catch (Exception ex) {
            err(ex, "unable to log request");
        }

        long startTime = System.currentTimeMillis();
        long sentBytesCount = originalRequest.body() == null? 0: originalRequest.body().contentLength();
        Response response = chain.proceed(originalRequest);

        NetworkPerformanceMeasureTools.RequestInfo requestInfo = new NetworkPerformanceMeasureTools.RequestInfo();
        requestInfo.executionTime = System.currentTimeMillis() - startTime;
        requestInfo.sentBytesCount = sentBytesCount;
        requestInfo.receivedBytesCount = response.body() == null? 0: response.body().contentLength();
        requestInfo.uri = originalRequest.urlString();

        StringBuilder sb = new StringBuilder();

        for (String headerName : response.headers().names()) {
            List<String> values = response.headers(headerName);
            for (String value : values) {
                sb.append(headerName).append(":").append(value).append("\n");
            }
        }

        final MediaType contentType = response.body().contentType();
        response = unzip(response);
        final byte[] body = response.body().bytes();
        response = response.newBuilder().body(new ResponseBody() {
            @Override
            public MediaType contentType() {
                return contentType;
            }

            @Override
            public long contentLength() {
                return body.length;
            }

            @Override
            public BufferedSource source() {
                return Okio.buffer(Okio.source(new ByteArrayInputStream(body)));
            }
        }).build();

        NetworkPerformanceMeasureTools.addRequestInfo(requestInfo);
        log("time: %d sent: %d received: %d request: %s  code: %d message: %s \n headers: %s \n",
                requestInfo.executionTime, requestInfo.sentBytesCount, requestInfo.receivedBytesCount,
                requestInfo.uri, response.code(), response.message(), sb);
        saveContent(body, requestInfo.uri);
        return response;
    }

    private Response unzip(final Response response) throws IOException {
        if (!"gzip".equalsIgnoreCase(response.header("Content-Encoding"))) {
            return response;
        }

        if (response.body() == null) {
            return response;
        }

        GzipSource responseBody = new GzipSource(response.body().source());
        Headers strippedHeaders = response.headers().newBuilder()
                .removeAll("Content-Encoding")
                .removeAll("Content-Length")
                .build();
        return response.newBuilder()
                .headers(strippedHeaders)
                .body(new RealResponseBody(strippedHeaders, Okio.buffer(responseBody)))
                .build();
    }

    private void saveContent(byte[] content, String url) throws IOException {
        int ind = url.indexOf("?");
        if (ind > 0) {
            url = url.substring(0, ind);
        }
        File f;
        if (interceptedContentDirectory == null) {
            f = new File(toFileName(System.nanoTime() + "_" + url) + ".html");
        } else {
            f = new File(interceptedContentDirectory, toFileName(System.nanoTime() + "_" + url) + ".html");
        }
        if (!f.createNewFile()) {
            throw new IOException("Unable to create file " + f.getAbsolutePath());
        }
        FileOutputStream stream = null;
        try {
            stream = new FileOutputStream(f);
            stream.write(content);
        } finally {
            Utils.safeClose(stream);
        }
    }

    private String toFileName(String url) {
        return url.replaceAll("[^a-z0-9_-]", "_");
    }
}
