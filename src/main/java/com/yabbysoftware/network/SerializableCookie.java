package com.yabbysoftware.network;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.List;

public class SerializableCookie implements Serializable {

    private static final long serialVersionUID = 1978557110274304124L;

    private transient HttpCookie cookie;

    public SerializableCookie(HttpCookie cookie) {
        this.cookie = cookie;
    }

    public HttpCookie getCookie() {
        return cookie;
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.defaultWriteObject();
        oos.writeObject(cookie.getName());
        oos.writeObject(cookie.getValue());
        oos.writeObject(cookie.getPath());
        oos.writeObject(cookie.getDomain());
        oos.writeObject(cookie.getComment());
        oos.writeObject(cookie.getCommentURL());
        oos.writeObject(cookie.getPortlist());
        oos.writeBoolean(cookie.getDiscard());
        oos.writeBoolean(cookie.getSecure());
        oos.writeLong(cookie.getMaxAge());
        oos.writeInt(cookie.getVersion());
    }

    private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
        ois.defaultReadObject();
        cookie = new HttpCookie((String) ois.readObject(), (String) ois.readObject());
        cookie.setPath((String) ois.readObject());
        cookie.setDomain((String) ois.readObject());
        cookie.setComment((String) ois.readObject());
        cookie.setCommentURL((String) ois.readObject());
        cookie.setPortlist((String) ois.readObject());
        cookie.setDiscard(ois.readBoolean());
        cookie.setSecure(ois.readBoolean());
        cookie.setMaxAge(ois.readLong());
        cookie.setVersion(ois.readInt());
    }

    public static List<SerializableCookie> toSerializable(List<HttpCookie> cookies) {
        List<SerializableCookie> res = new ArrayList<>(cookies.size());
        for (HttpCookie cookie : cookies) {
            res.add(new SerializableCookie(cookie));
        }
        return res;
    }

    public static List<HttpCookie> fromSerializable(List<SerializableCookie> cookies) {
        List<HttpCookie> res = new ArrayList<>(cookies.size());
        for (SerializableCookie cookie : cookies) {
            res.add(cookie.getCookie());
        }
        return res;
    }
}
